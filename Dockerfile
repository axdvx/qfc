
FROM debian:9.13-slim
RUN apt-get update && apt-get install -y openjdk-8-jdk-headless && rm -rf /var/lib/apt/lists/*

ADD assets/qf-dist.tgz /
VOLUME ["/qf-dist/data"]
stopsignal 2
EXPOSE 8080 4040

CMD ["/qf-dist/startQF", "-a", "0.0.0.0"]
