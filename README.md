
QF Console
---
A Spark-SQL IDE for the Browser


## Features

- Deployable on existing Spark-Clusters or as standalone application on the Desktop.
- Schema Tree View
- Schema / Table / Column Properties with graphical Histograms 
- CSV Table Upload
- JDBC Import from other Database
- SQL Editor with grammar checking and contextual error reporting / auto completion
- Result Table View with CSV and XLSX Export
- Pivot Mode with Drag and Drop
- WebGL 3D Pivot Scene
- Embedded Git Server for SQL source files


##### Powered by
- Apache Spark
- Apache Arrow
- cats http4s circe fs2
- zio
- Vue
- and others


# Quick Start

The *Desktop Distribution*
is intended for deployment on a personal workstation. It includes the full Spark libraries as well as
any other libraries in order to operate without further installation. (though Java-8 Runtime is required)

Although primarily intended for single user usage the Desktop Installation can expose its Web
interface to other users as well as LDAP authorization functionality.

### Desktop Mode

- Download *qf-dist-zip* from [Releases page](https://gitlab.com/axdvx/qfc/-/packages/3461726) and unpack it to some folder. This creates:

```sh
.
├── lib
│   └── *.jar
├── mkPassword
├── mkPassword.cmd
├── qf-svr-1.0.jar
├── startQF
└── startQF.cmd

```

- Run *startQF* in terminal or console. 
- an additional directory *data* is created with default configuration and tablespace
- Open http://localhost:8080 in the browser
- The preconfigured login is: *demo/demo* (see configuration to change this)
- Press the arrow icon next to the default schema for CSV - Upload (e.g. [customer.csv](https://github.com/neo4j-examples/neo4j-foodmart-dataset/tree/master/data)) 
- Enter table name and press ok. The table will subsequently appear in the schema node. 
- Use the preview button next to the table name to display the contents
- Go to Pivot Mode, use drag-and-drop from the columns to build a pivot table
- Click icon in upper left corner to show the pivot table in a 3D WebGL-Scene   
