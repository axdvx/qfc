/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package de.axiohq.spark.svr

import com.google.flatbuffers.FlatBufferBuilder
import fs2._
import org.apache.arrow.flatbuf.MessageHeader
import org.apache.arrow.vector.ipc.message.MessageSerializer
import org.apache.spark.TaskContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.execution.SparkPlan
import org.apache.spark.sql.types.StructType

object ArrowBatchStreamWriter {
	type ArrowStream[AppTask[_]] = fs2.Stream[AppTask, Byte]

	implicit class converters(v:Int) {
		def getBytes = Array[Byte]((v>>>0).toByte, (v>>>8).toByte, (v>>>16).toByte, (v>>>24).toByte)
	}
	
	def apply[F[_]](schema: StructType, timeZoneId: String) = {
		val arrowSchema = ArrowFunctions.toArrowSchema(schema, timeZoneId)
		val builder = new FlatBufferBuilder;
		val schemaOffset = arrowSchema.getSchema(builder);
		val serializedMessage = MessageSerializer.serializeMessage(builder, MessageHeader.Schema, schemaOffset, 0);

		val messageLength = serializedMessage.remaining();
		
		val pml = if ((messageLength + 4) % 8 != 0) messageLength + 8 - (messageLength + 4) % 8 else messageLength
		val intbuf = pml.getBytes
		val padn = pml - messageLength

		fs2.Stream.chunk(Chunk.bytes(intbuf)) ++
			fs2.Stream.chunk(Chunk.byteBuffer(serializedMessage)) ++ 
			fs2.Stream.chunk(Chunk.bytes(Array.ofDim[Byte](padn)))
	}
	
	def writeBatches[F[_]](stream: ArrowStream[F], arrowBatchIter: Iterator[Array[Byte]]): ArrowStream[F] = {
		var r = stream
		arrowBatchIter.foreach(p => {r = r ++ Stream.chunk(Chunk.bytes(p))})
		r
	}
	
	def end[F[_]]: ArrowStream[F] = Stream.chunk(Chunk.bytes(0.getBytes))
}


object SparkExecution {
	def run(ds: Dataset[Row]): Array[Array[Array[Byte]]] = {
		val toArrowBatchRdd = ds.getClass.getDeclaredMethod("toArrowBatchRdd", classOf[SparkPlan])
		toArrowBatchRdd.setAccessible(true)

		ArrowFunctions.sqlExecution(ds) {
			ds.queryExecution.executedPlan.resetMetrics
			val arrowBatchRdd = toArrowBatchRdd.invoke(ds, ds.queryExecution.executedPlan).asInstanceOf[RDD[Array[Byte]]]

			val nparts = arrowBatchRdd.partitions.length
			val results = new Array[Array[Array[Byte]]](nparts)

			ds.sparkSession.sparkContext.runJob(arrowBatchRdd,
				(ctx: TaskContext, it: Iterator[Array[Byte]]) => it.toArray,
				0 until nparts,
				(index: Int, batches: Array[Array[Byte]]) => results(index) = batches)
			results
		}
	}
	
	def apply[F[_]](ds: Dataset[Row]): Stream[F, Chunk[Byte]] = {
		val timeZoneId = ds.sparkSession.sessionState.conf.sessionLocalTimeZone
		val metaData = ArrowBatchStreamWriter[F](ds.schema, timeZoneId)

		val data = SparkExecution.run(ds).flatten
			.map(Chunk.bytes)
			.map(Stream.chunk)
			.foldLeft(metaData)(_++_) ++ ArrowBatchStreamWriter.end
		
		data.chunks
	}
}
