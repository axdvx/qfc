/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr

import cats.effect.Blocker
import de.axiohq.spark.svr.cluster._
import de.axiohq.spark.svr.config._
import de.axiohq.spark.svr.http4s.{ QFDeployment, UndertowBuilder }
import zio._
import zio.interop.catz._

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext

object Main extends App with Logging {
	System.setProperty("org.jboss.logging.provider", "slf4j")

	implicit val ec = ExecutionContext.global
	implicit val blocker = Blocker.liftExecutionContext(ec)

	implicit class UndertowHelper[F[_]](ub: UndertowBuilder[F]) {
		@tailrec
		final def bindListeners(listeners: Seq[Listener]): UndertowBuilder[F] = {
			listeners match {
				case Seq() => ub
				case _ =>
					val ub1 = listeners.head match {
						case n: StandardListener => ub.bindHttp(n.port, n.host)
						case n: SecureListener => ub.bindHttps(n.port, n.host, n.keystore, n.storepw, n.name)
					}
					ub1.bindListeners(listeners.tail)
			}
		}
	}

	def server: ZIO[AppEnv, Throwable, Unit] = ZIO.runtime[AppEnv]
		.flatMap {
			implicit rts =>
				for {
					cfg <- Configuration.conf
					server <- UndertowBuilder[AppTask]
						.bindListeners(cfg.server.listeners)
						.addDeployment(QFDeployment.Main, Map("/api/*" -> QFServices()))
						.addDeployment(QFDeployment.Repo)
						.withBanner(banner)
						.serve
						.compile[AppTask, AppTask, Any]
						.drain
				} yield server
		}

	def failWithReport(x: Throwable): ExitCode = {
		println()
		println(x)
		println()
		ExitCode.failure
	}

	def run(args: List[String]) = {
		CommandLine(args) match {
			case None => ZIO.succeed(ExitCode.failure)
			case Some(cmdLine) => {
				val layer: ZLayer[ZEnv, Nothing, Configuration with Cluster] = ConfigService.live(cmdLine) >+> SparkService.live
				server.provideSomeLayer[ZEnv](layer).fold(failWithReport, Unit => ExitCode.success)
			}
		}
	}

	private val banner = """
           ____
    ____ _/ __/ ______   _______
   / __ `/ /_  / ___/ | / / ___/
  / /_/ / __/ (__  )| |/ / /    
  \__, /_/   /____/ |___/_/    v1.0     
    /_/                          
	""".split("\n").toList
}

