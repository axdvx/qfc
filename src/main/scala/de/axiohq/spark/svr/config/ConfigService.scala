/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.config

import de.axiohq.spark.svr.model.Settings

import com.typesafe.config.ConfigFactory
import com.typesafe.config.ConfigRenderOptions
import com.typesafe.config.ConfigResolveOptions
import scala.util.Try
import zio._
import java.io.FileWriter
import pureconfig._
import pureconfig.generic.auto._


class ConfigService(clc : CmdLineOpts) extends Configuration.Service {
	private val optsp = ConfigResolveOptions.defaults.setUseSystemEnvironment(true).setAllowUnresolved(true)
	private val defaultCF: CF = CF (
		server =	ServerCF(listeners = Seq(StandardListener(host = "localhost", port = 8080)), secured = Some(true)), 
		spark  =	SparkCF(master = "local[*]", configs = Seq(("spark.sql.statistics.histogram.enabled","true"))), 
		ldap   =	None, 
		users  =	Seq(UserCF("demo","{SSHA}CRyyRY0Iqp+wVBO8JGzFNMcxflLYDYnv", Seq("query"))))
	
	def setPort(listener: Listener, port: Int) = {
		listener match {
			case n: StandardListener => n.copy(port = port)
			case n: SecureListener => n.copy(port = port + 1)
		}
	}
	
	def setHost(listener: Listener, host: String) = {
		listener match {
			case n: StandardListener => n.copy(host = host)
			case n: SecureListener => n.copy(host = host)
		}
	}

	def getConf = ZIO.effect {
			implicit val serverReader = ConfigReader[ServerCF]
				.map(svr => clc.host.map(v => svr.copy(listeners = svr.listeners.map(setHost(_, v)))).getOrElse(svr))
				.map(svr => clc.port.map(v => svr.copy(listeners = svr.listeners.map(setPort(_, v)))).getOrElse(svr))
				.map(svr => clc.secured.map(v => svr.copy(secured = Some(v))).getOrElse(svr))
				
			if (!Settings.configFile.toFile.exists) {
				Settings.basedir.toFile.mkdirs
				val writer = ConfigWriter[CF].to(defaultCF)
				val opts = ConfigRenderOptions.defaults().setComments(false).setOriginComments(false).setJson(false).setFormatted(true)
				var fwr: FileWriter = null
				try {
					fwr = new FileWriter(Settings.configFile.toFile)
					fwr.write(writer.render(opts))
				}
				finally {
					if(fwr != null) fwr.close
				}
			}
			
			val cf0 = ConfigFactory.parseFile(Settings.configFile.toFile).resolve(optsp)
			ConfigSource.fromConfig(cf0).loadOrThrow[CF]
		}

}
object ConfigService {
	def live(clc: CmdLineOpts): Layer[Nothing, Configuration] = ZLayer.succeed(new ConfigService(clc))
}
