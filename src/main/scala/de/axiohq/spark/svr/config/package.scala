/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr

import zio._

package object config {
	type Configuration = Has[Configuration.Service]

	case class CmdLineOpts(secured: Option[Boolean] = None, passwd: Option[Boolean] = None, host: Option[String] = None, port: Option[Int] = None)

	case class LdapGroupCF(allowedRoles: Seq[String], baseDN: String, roleName: String, memberAttribute: String)

	case class LdapCF(url: String, baseDN: String, userAttribute: String, extraUsers: Option[Seq[String]], groups: Option[LdapGroupCF])

	case class UserCF(name: String, password: String, roles: Seq[String])

	sealed trait Listener {
		val host: String
		val port: Int
	}
	case class StandardListener(host: String, port: Int) extends Listener

	case class SecureListener(host: String, port: Int, keystore: String, storepw: String, name: String) extends Listener
	
	case class ServerCF(listeners: Seq[Listener], secured: Option[Boolean])

	case class SparkCF(master: String, configs: Seq[Tuple2[String,String]])

	case class CF(server: ServerCF, spark: SparkCF, ldap: Option[LdapCF], users: Seq[UserCF])

	object Configuration {
		trait Service {
			def getConf: Task[CF]
		}
		def conf: RIO[Configuration, CF] = ZIO.accessM(_.get.getConf)
	}
}
