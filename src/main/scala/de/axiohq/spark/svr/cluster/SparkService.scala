/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.cluster

import java.util.concurrent.ConcurrentHashMap

import de.axiohq.spark.svr._
import de.axiohq.spark.svr.config._
import de.axiohq.spark.svr.model.Settings
import org.apache.spark.sql.SparkSession
import zio.{Layer, ZLayer, _}

import scala.collection.JavaConverters._

object SparkService extends Cluster.Service {

	val cookie = Settings.distPath.map(p=>scala.util.Random.alphanumeric.take(40).mkString)
	
	private[this] val sessions = new ConcurrentHashMap[String, SparkSession].asScala

	private def mkSpark(cfg: SparkCF) = {
		val builder = SparkSession.builder.appName("qf-server").enableHiveSupport
		val b = Settings.distPath match {
			case Some(path) => {
				val bldr = builder.master(cfg.master)
					.config("spark.scheduler.mode","FAIR") 
					.config("spark.local.dir", s"${path}/tmp/scratch")
					.config("spark.eventLog.dir", s"${path}/tmp/events")
					.config("spark.sql.warehouse.dir", s"${path}/dwh")
					.config("javax.jdo.option.ConnectionDriverName", "org.h2.Driver")
					.config("javax.jdo.option.ConnectionURL", s"jdbc:h2:${path}/db/qf;SCHEMA=spark")
					.config("javax.jdo.option.ConnectionUserName", "qf")
					.config("javax.jdo.option.ConnectionPassword", "qf")
					.config("datanucleus.autoCreateSchema", true)
					.config("datanucleus.schema.autoCreateTables", true)
					.config("spark.sql.adaptive.enabled", true)
					.config("spark.ui.filters", "de.axiohq.spark.svr.model.UIFilter")
					.config("spark.de.axiohq.spark.svr.model.UIFilter.param.secret", cookie.get)
				cfg.configs.foldLeft(bldr)((b,v) => b.config(v._1, v._2))
				
			}
			case None => {
					cfg.configs.foldLeft(builder)((b,v) => b.config(v._1, v._2))
					builder
			}
		}
		val r = b.getOrCreate
//		r.sparkContext.setLogLevel("warn")
		r
	}

	private def stopSpark(spark: SparkSession) = {
		spark.stop
	}

	def getSpark(user0: Option[String]): ZIO[AppEnv, Throwable, SparkSession] = {
		val user = user0.getOrElse("anonymous")
		for {
			cfg <- Configuration.conf
			session <- ZIO.succeed {
				val session1 = sessions.getOrElseUpdate(user, {
					val r = mkSpark(cfg.spark)
					r.sparkContext.setJobGroup(user, "", true)
					r.conf.set("qf.limit", 1000)
					r
				})
				session1.sparkContext.setJobGroup(user, "", true)
				session1
			}
		} yield session
	}

	//	val managed = Managed.makeEffect(ZIO.effectTotal(this))(p => p.stopSpark)
	//	val live: Layer[Nothing, Cluster] = ZLayer.fromManaged(managed)
	val live: Layer[Nothing, Cluster] = ZLayer.succeed(this)

}
