/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr

import java.io.File
import java.nio.file.Paths

import cats.Semigroupal
import cats.effect.Blocker
import cats.instances.option._
import de.axiohq.spark.svr.cluster._
import de.axiohq.spark.svr.http4s.Middlewares._
import de.axiohq.spark.svr.http4s.encoders.sparkEncoders._
import de.axiohq.spark.svr.model.{QFOperations, Repository}
import de.axiohq.spark.svr.model.RequestUtils._
import io.circe.Encoder
import io.circe.generic.auto._
import org.http4s._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.headers._
import org.http4s.implicits._
import org.http4s.multipart.{Multipart, Part}
import zio._
import zio.interop.catz._

object QFServices extends QFOperations with Http4sDsl[AppTask] {

	object qParam extends QueryParamDecoderMatcher[String]("q")
	object fParam extends QueryParamDecoderMatcher[String]("f")
	object nParam extends QueryParamDecoderMatcher[String]("n")
	object tblParam extends QueryParamDecoderMatcher[String]("tbl")
	object npParam extends OptionalQueryParamDecoderMatcher[Int]("np")
	object fsParam extends OptionalQueryParamDecoderMatcher[String]("fs")

	object msgParam extends OptionalQueryParamDecoderMatcher[String]("msg")
	object urlParam extends QueryParamDecoderMatcher[String]("url")
	object unParam extends QueryParamDecoderMatcher[String]("u")
	object pnParam extends QueryParamDecoderMatcher[String]("p")
	
	object limitParam extends OptionalQueryParamDecoderMatcher[Boolean]("limited")
	object dbgParam extends OptionalQueryParamDecoderMatcher[Boolean]("dbg")

	def apply()(implicit blocker: Blocker, rt: Runtime[AppEnv]) = Normalize(routes)

	implicit def circeJsonEncoder[A](implicit decoder: Encoder[A]): EntityEncoder[AppTask, A] = jsonEncoderOf[AppTask, A]
	implicit def streamEncoder[A](implicit decoder: Encoder[A]): EntityEncoder[AppTask, fs2.Stream[AppTask, A]] = EntityEncoder.streamEncoder[AppTask, A]
	
	def fileExt[AppTask[_]](part: Part[AppTask]) = part.headers.get(`Content-Disposition`)
		.flatMap(_.parameters.get("filename"))
		.flatMap(_.split("\\.").filter(_.length>1).lastOption.map(p => s".${p}"))

	def withCookie[AppTask[_]] = (response: Response[AppTask]) => {
		SparkService.cookie
			.map(p=>response.addCookie(ResponseCookie(name = "SPARKSESSION", content = p, path = Some("/"))))
			.getOrElse(response)
	}
	
	def routes(implicit blocker: Blocker, rt: Runtime[AppEnv]) = HttpRoutes.of[AppTask] {
		case req @ GET  -> Root / "v1" / "dict" => Ok(hello(req)).map(withCookie)
		case req @ GET  -> Root / "v1" / "dict" / "reset" => Ok(cancelJobs(req))
		case req @ GET  -> Root / "v1" / "dict" / "status" => Ok(status(req))
		case req @ GET  -> Root / "v1" / "dict" / "schema" => Ok(getSchema(req))

		case req @ POST -> Root / "v1" / "dict" / "schema" => {
			val r = for {
				txt <- req.bodyText.compile.string
				data <- setSchema(req)(txt)
			} yield data
			Ok(r)
		}
		
		case req @ GET  -> Root / "v1" / "dict" / "limit" => Ok(getLimit(req))

		case req @ POST -> Root / "v1" / "dict" / "limit" => {
			val r = for {
				txt <- req.bodyText.compile.string
				data <- setLimit(req)(txt)
			} yield data
			Ok(r)
		}

		case req @ GET  -> Root / "v1" / "dict" / "schemas"  => Ok(schemas(req))
			
		case req @ GET  -> Root / "v1" / "dict" / "schemas" / name => Ok(schema(req)(name))

		case req @ POST  -> Root / "v1" / "dict" / "schemas" / name :? tblParam(tbl) :? npParam(np) :? fsParam(fs) => {
			def runPart(stream: fs2.Stream[AppTask, Byte], ext: String) = ZIO.succeed(File.createTempFile("qf-", ext))
				.bracket(f => ZIO.succeed(f.delete)) { f => {
						val job = stream.through(fs2.io.file.writeAll(Paths.get(f.toString), blocker)).compile.drain
						val z = rt.unsafeRunSync(job)
						importCSV(req)(f, name, tbl, np, fs)
					}
				}

			req.decode[Multipart[AppTask]] { mp =>
					mp.parts.filter(_.headers.exists(_.value.contains("filename"))).headOption
							.flatMap(part => Semigroupal[Option].product(Some(part), fileExt(part)))
							.map(partx => runPart(partx._1.body, partx._2))
							.map(p=> p.foldM(a => NotAcceptable(a), b => Ok(b)))
							.getOrElse(NotAcceptable("nothing"))
			}
		}

		case req @ GET  -> Root / "v1" / "dict" / "schemas" / schema / tbl => Ok(tableInfo(req)(schema, tbl))
			
		case req @ GET  -> Root / "v1" / "repo" / "get" / name => 
				Repository.getFile[AppTask](req.user, name).foldM(NotAcceptable(_), Ok(_))
			
		case req @ GET  -> Root / "v1" / "repo" / "ren"  :? fParam(f) :? nParam(n) => 
				Repository.rmoveFile[AppTask](req.user, f, Option(n)).foldM(NotAcceptable(_), Ok(_))
			
		case req @ GET  -> Root / "v1" / "repo" / "del"  :? fParam(f) => 
				Repository.rmoveFile[AppTask](req.user, f, None).foldM(NotAcceptable(_), Ok(_))
			
		case req @ GET  -> Root / "v1" / "repo" / "list" => 
				Repository.listRepo(req.user).foldM(NotAcceptable(_), Ok(_))

		case req @ POST -> Root / "v1" / "repo" / "set" / name => {
			val r = for {
				txt <- req.bodyText.compile.string
				data <- Repository.putFile[AppTask](req.user, name, txt, None)
			} yield data
			Ok(r)
		}
		
		case req @ POST -> Root / "v1" / "repo" / "com" / name :? msgParam(msg) => {
			val r = for {
				txt <- req.bodyText.compile.string
				data <- Repository.putFile[AppTask](req.user, name, txt, msg)
			} yield data
			Ok(r)
		}

		case req @ GET -> Root / "v1" / "dict" / "sqlx" :? qParam(q) +& limitParam(limited) +& dbgParam(dbg) => {
			val start = System.currentTimeMillis
				val r = sqlx(req)(q, limited.getOrElse(false))
				r.foldM(
					x => x match {
					case x: org.apache.spark.sql.AnalysisException => NotAcceptable(report(x))
					case x: Throwable => InternalServerError(report(x))
					}, 
					p => Ok(p, Header("timing", ""+(System.currentTimeMillis-start))))
		}
		
		case req @ GET -> Root / "v1" / "dict" / "importJdbc" :? qParam(q) +& urlParam(url) +& unParam(u) +& pnParam(p) +& tblParam(tbl) => {
			val start = System.currentTimeMillis
			val r = importJDBC(req)(q, url, u, p, tbl)
			r.foldM(x => InternalServerError(report(x)), p =>  Ok(p, Header("timing", ""+(System.currentTimeMillis-start))))
		}
		
	} orNotFound
}
