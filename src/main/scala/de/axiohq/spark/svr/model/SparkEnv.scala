/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.model
import de.axiohq.spark.svr.cluster._
import de.axiohq.spark.svr.http4s.UndertowBuilder
import org.http4s.Request

object SparkEnv {
	def getUser[AppTask[_]](req: Request[AppTask]): Option[String] = req.attributes.lookup(UndertowBuilder.utsKey).flatten.map(_.getName)

	def session[AppTask[_]](req: Request[AppTask]) = {
		Cluster.spark(getUser(req))
	}

	def cancelJobs[AppTask[_]](req: Request[AppTask]) = for {
		spark <- session(req)
		user = getUser(req)
		data = spark.sparkContext.cancelJobGroup(user.getOrElse("anonymous"))
	} yield Unit
}
