/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.model

import javax.servlet.http.{HttpServletRequest,HttpServletResponse, Cookie}
import javax.servlet.{Filter, ServletRequest, ServletResponse, FilterChain, FilterConfig}

class UIFilter extends Filter {
	var sec: Option[String] = None
	override def init(cfg: FilterConfig) = 
		 sec = Option(cfg.getInitParameter("secret"))
	 
	override def destroy = Unit
	def doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain): Unit = {
		val srq = request.asInstanceOf[HttpServletRequest]
		val srp = response.asInstanceOf[HttpServletResponse]
		
		Option(srq.getCookies).getOrElse(Array.empty[Cookie])
			.filter(_.getName == "SPARKSESSION")
			.flatMap(p => sec.map(_.equals(p.getValue)))
			.headOption
		match {
			case 
				Some(_) => chain.doFilter(request, response)
				case _ => srp.sendError(HttpServletResponse.SC_UNAUTHORIZED)
		}
	}
}
