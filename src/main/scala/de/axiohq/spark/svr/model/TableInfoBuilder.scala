/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.model

import java.time.Instant
import java.time.LocalDateTime
import java.util.TimeZone
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.TableIdentifier
import de.axiohq.spark.svr.api._
import org.apache.spark.sql.catalyst.catalog.CatalogStatistics

class TableInfoBuilder(implicit spark: SparkSession) {
	import TableInfoBuilder._
	def create(db:String, table:String): TableInfo = {
		val catalogTable = spark.sessionState.catalog.getTableMetadata(TableIdentifier(table,Option(db)))

		val partitions = catalogTable.partitionColumnNames.toSet
		val colInfos = catalogTable.schema.fields
				.map(p => ColumnInfo(p, 
				partitions contains p.name,
				catalogTable.stats.map(_.colStats).flatMap(_.get(p.name))))

		val ddlTime = catalogTable.properties.get("transient_lastDdlTime")
			.map(_.toLong).map(TableInfoBuilder.toLocalDateTime(_)).map(_.toString).map(_.replace("T", " "))
		val createTime = TableInfoBuilder.toLocalDateTime3(catalogTable.createTime).toString.replace("T", " ")
		val fmt = catalogTable.storage.serde.flatMap(_.split("\\.").lastOption).map(_.replaceAll("(?i)serde$", ""))

		TableInfo(catalogTable.identifier,
			catalogTable.tableType.name,
			fmt,
			catalogTable.provider,
			catalogTable.createVersion,
			catalogTable.owner,
			createTime,
			ddlTime,
			catalogTable.stats.map(p => TableStats(p.rowCount, p.sizeInBytes)),
			colInfos)
	}
}

object TableInfoBuilder {
	def apply(implicit spark: SparkSession) = new TableInfoBuilder
	def toLocalDateTime(tm: Long) = LocalDateTime.ofInstant(Instant.ofEpochSecond(tm), TimeZone.getTimeZone("UTC").toZoneId)
	def toLocalDateTime3(tm: Long) = LocalDateTime.ofInstant(Instant.ofEpochMilli(tm), TimeZone.getTimeZone("UTC").toZoneId)
}
