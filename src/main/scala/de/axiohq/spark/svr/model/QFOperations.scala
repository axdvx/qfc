/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.model
import de.axiohq.spark.svr.config._
import zio.ZIO
import de.axiohq.spark.svr._


import java.io.File

import de.axiohq.spark.svr.http4s.encoders._
import de.axiohq.spark.svr.{SparkExecution, Logging}

import org.apache.spark.sql.AnalysisException
import org.apache.spark.sql.types.StructType

import org.http4s.Request

import scala.util.Failure
import scala.util.Try

import RequestUtils._

trait QFOperations extends Logging {
	case class Status(limit: Option[Int], current: String, schemas: Seq[String])

	case class report(message: String, line: Option[Int], pos: Option[Int])

	object report {
		def apply(x: AnalysisException): report = report(x.message, x.line, x.startPosition)
		def apply(x: Throwable): report = report(s"${x.getClass.getSimpleName}: ${x.getMessage}", None, None)
	}

	def hello[F[_]](request: Request[F])  = for {
			spark	<- SparkEnv.session(request)
			data	= (spark.version, spark.sparkContext.master, spark.sparkContext.uiWebUrl, spark.sparkContext.defaultParallelism)
	} yield data

	def setLimit[F[_]](request: Request[F])(limit: String) = for {
			spark	<-	SparkEnv.session(request)
			data	=	Try { Option(limit).map(_.toInt) }.toOption.flatten match {
			case Some(x) => spark.conf.set("qf.limit", x)
			case None => spark.conf.unset("qf.limit")
		}
	} yield Unit

	def getLimit[F[_]](request: Request[F]) = for {
			spark		<- SparkEnv.session(request)
			limit		= spark.conf.getOption("qf.limit").map(_.toInt)
	} yield limit

	def schemas[F[_]](request: Request[F]) = for {
			spark		<- SparkEnv.session(request)
			data		=  spark.catalog.listDatabases.take(Integer.MAX_VALUE)
	} yield data

	def status[F[_]](request: Request[F]) = for {
			spark		<- SparkEnv.session(request)
			schemas		=  spark.catalog.listDatabases.take(Integer.MAX_VALUE).map(_.name).toSeq
			status		=  Status(spark.conf.getOption("qf.limit").map(_.toInt), spark.catalog.currentDatabase, schemas)
	} yield status

	def getSchema[F[_]](request: Request[F]) = for {
			spark		<- SparkEnv.session(request)
	} yield spark.catalog.currentDatabase

	def setSchema[F[_]](request: Request[F])(name: String) = for {
			spark		<- SparkEnv.session(request)
			_			=  spark.catalog setCurrentDatabase name
	} yield spark.catalog.currentDatabase

	def schema[F[_]](request: Request[F])(name: String) = for {
			spark		<- SparkEnv.session(request)
	} yield spark.catalog.listTables(name).take(Integer.MAX_VALUE)

	def tableInfo[F[_]](request: Request[F])(schema: String, table: String) =  for {
			spark		<- SparkEnv.session(request)
	} yield TableInfoBuilder(spark).create(schema, table)

	def importJDBC[F[_]](req: Request[F])(query: String,
			url: String,  user: String, passwd: String,
			tbl: String) = {
		import scala.collection.JavaConverters._
		for {
			spark	<- SparkEnv.session(req)
			r		<- ZIO.effect {
						val props = new java.util.Properties
						props.putAll(Map("user"->user, "password"->passwd, "fetchsize"->"1000").asJava)
						spark.read.jdbc(url, s"(${query}) t", props).write.saveAsTable(s"${tbl}")
					}
		} yield r
	}
	
	def importCSV[F[_]](req: Request[F])(input: File, schema: String, name: String, np: Option[Int], fs: Option[String]) = {
		for {
			spark	<- SparkEnv.session(req)
			r		<- ZIO.effect {
						val df1 = spark.read
							.options(Map("header" -> "true", "inferSchema" -> "true", "parserLib" -> "univocity", "delimiter" -> fs.getOrElse(",")))
							.csv(input.getPath)

						val s1 = new StructType(df1.schema.fields.map(p=>p.copy(name=p.name.toLowerCase)))
						val df = spark.createDataFrame(df1.rdd, s1)

						val dfw = np.map(df.coalesce).getOrElse(df)
						dfw.write.saveAsTable(s"${schema}.${name}")

						report(s"Saved to Table: ${schema}.${name}", None, None)
					}
		} yield r
	}

	def cancelJobs[F[_]](req: Request[F]) = SparkEnv.cancelJobs(req)

	def sqlx[F[_]](req: Request[F])(query: String, limited: Boolean = true) = {
		val sql = query.trim
		LOG.info(s"running[lim: ${limited}]: ${sql}")
		for {
			spark	<- SparkEnv.session(req)
			lim		<- getLimit(req)
			arrow   <- ZIO.fromTry {
						Try {
								val ds0 = spark.sql(query)
								val ds =  lim.filter(_ => limited).map(ds0.limit(_)).getOrElse(ds0)
								SparkExecution[F](ds)
							}.recoverWith {
								case p : Throwable => 
									LOG.error(s"SQL Error:\n\n${query}\n\n${p.getMessage}")
									Failure(p)
						}
			}
		} yield arrow
	}

}
