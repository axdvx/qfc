/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.model

import de.axiohq.spark.svr.Logging

import java.nio.file.{Files, StandardOpenOption}
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.stream.Collectors

import cats.effect.{Blocker, ContextShift, Sync}
import zio._

import org.apache.commons.io.FileUtils
import org.eclipse.jgit.api.{Git, ResetCommand}
import org.eclipse.jgit.lib.Constants
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.eclipse.jgit.treewalk.TreeWalk
import org.eclipse.jgit.treewalk.filter.PathSuffixFilter

import scala.collection.JavaConverters._

object Repository extends Logging {
	val repoPath = Settings.repoPath
	
	def timestamp: String = LocalDateTime.now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"))

	private[model] def openRepository(user: Option[String]): org.eclipse.jgit.lib.Repository = {
		val path = repoPath.resolve(user.getOrElse("__DEFAULT__"))
		val dir = path.toFile
		if (!dir.exists) if (!dir.mkdirs) throw new Exception(s"Cannot create ${dir}")
		val builder = new FileRepositoryBuilder().setGitDir(dir)
		if (builder.getIndexFile == null)
			Git.init.setBare(false).setDirectory(dir).call.getRepository
		else
			builder.readEnvironment.build
	}

	def gitM(user: Option[String]): ZManaged[Any, Throwable ,Git] =
		ZManaged.makeEffect {
			val path = repoPath.resolve(user.getOrElse("__DEFAULT__"))
			val repo = openRepository(user)
			val git = new Git(repo)
			
			if (repo.resolve(Constants.HEAD) == null) {
				val prefs = path.resolve("preferences.json")
				FileUtils.touch(prefs.toFile)
				git.add.addFilepattern(prefs.getFileName.toString).call
				git.commit.setMessage(s"added ${prefs.getFileName.toString}").call
			}
			git
		} { git=>ZIO.effect(git.close) }

	
	def getFile[AppTask[_]: Sync: ContextShift](user: Option[String], path: String)(implicit blocker: Blocker): Task[String] = {
		gitM(user).use { git => 
			ZIO.effect {
				var reader: java.io.BufferedReader = null
				try {
//					git.reset.setMode(ResetCommand.ResetType.HARD).call
					val fpath = git.getRepository.getWorkTree.toPath.resolve(path)
					reader = Files.newBufferedReader(fpath)
					reader.lines.collect(Collectors.joining("\n","","\n"))
				}
				finally { Option(reader).map(_.close)}
			}
		}
	}

	def rmoveFile[AppTask[_]](user: Option[String], path: String, path2: Option[String] = None): Task[String] = {
		gitM(user).use { git =>
			ZIO.effect {
				val wtree = git.getRepository.getWorkTree
				val msg = path2 match {
					case Some(fn) => 
						FileUtils.moveFile(wtree.toPath.resolve(path).toFile, wtree.toPath.resolve(fn).toFile)
						git.add.addFilepattern(fn).call
						s"renamed $path to $fn ($timestamp)"
					case None => 
						wtree.toPath.resolve(path).toFile.delete
						s"removed $path ($timestamp)"
				}
				git.rm.addFilepattern(path).call
				if (git.status.call.hasUncommittedChanges) {
					LOG.info(s"committing: $msg")
					git.commit.setMessage(msg).call
					git.log.call.asScala.head.getFullMessage
				}
				else s""
			}
		}
	}
	
	def putFile[AppTask[_]](user: Option[String], path: String, txt: String, message: Option[String]): Task[String] = {
		val resources = for {
			git		<- gitM(user)
			fpath	=  git.getRepository.getWorkTree.toPath.resolve(path)
			file	<- ZManaged.makeEffect {
							Files.newBufferedWriter(fpath, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE)
					   } {p => ZIO.effect(p.close)}
		} yield (git, file)

		resources.use { case (git, file) =>
				ZIO.effect {
					file.write(txt)
					file.flush
					message.map { msg =>
						git.add.addFilepattern(path).call
						if (git.status.call.hasUncommittedChanges) {
							LOG.info(s"committing: ${path}")
							git.commit.setMessage(msg).call
						}
					git.log.call.asScala.head.toString
					}.getOrElse(s"$path saved")
				}
		}
	}

	def listRepo(user: Option[String]): Task[Seq[String]] = {
		gitM(user).use { git => 
			ZIO.effect {
				val repo = git.getRepository
				val lastc = repo.resolve(Constants.HEAD)
				val tree = new RevWalk(repo).parseCommit(lastc).getTree
				val walk = new TreeWalk(repo)
				walk.setFilter(PathSuffixFilter.create("sql"))
				walk.addTree(tree)
				walk.setRecursive(false)
				Iterator.continually(walk).takeWhile(_.next).map(_.getNameString).toSeq
			}
		}
	}
}

