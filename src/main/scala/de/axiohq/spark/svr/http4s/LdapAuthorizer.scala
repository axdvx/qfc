/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.http4s


import io.undertow.security.idm.Account
import io.undertow.security.idm.Credential
import io.undertow.security.idm.PasswordCredential
import java.util.Hashtable
import javax.naming.Context
import javax.naming.NamingEnumeration
import javax.naming.directory.InitialDirContext
import javax.naming.directory.SearchControls
import javax.naming.directory.SearchResult

import zio._
import de.axiohq.spark.svr._
import de.axiohq.spark.svr.config._

object LdapAuthorizer extends QFAuthorizer {
	def checkGroups(ctx: InitialDirContext, cfg: LdapGroupCF, userDN: String): Seq[String] = {
		val gf = cfg.allowedRoles.map(p => s"(${cfg.roleName}=$p)").mkString
		val filter = s"(&(|${gf})(${cfg.memberAttribute}=${userDN}))"
		
		val ctrls = new SearchControls
		ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE)
		
		val n: NamingEnumeration[SearchResult]= ctx.search(cfg.baseDN, filter, ctrls)
		
		Iterator.continually(n).takeWhile(_.hasMore)
			.map(_.next.getAttributes.get(cfg.roleName).toString)
			.toSeq
	}

	def verify(id: String, credential: Credential)(implicit rt: Runtime[AppEnv]): Either[String, Either[String, Account]] = {
		LOG.info("Authentication: running LDAP")
		var ctx: InitialDirContext = null;
		try {
			val ldapCF1 = rt.unsafeRun[Throwable, Option[LdapCF]](Configuration.conf.map(_.ldap))

			val ldapCF = ldapCF1 match {
				case None => throw new Exception("No LDAP configration")
				case Some(cf) => cf
			}

			val url = ldapCF.url
			val userAttribute = ldapCF.userAttribute
			val baseDN = ldapCF.baseDN

			val env = new Hashtable[String, String]
			env.put(Context.PROVIDER_URL, ldapCF.url.trim)
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory")

			ctx = new InitialDirContext(env)
			var ctrls = new SearchControls
			ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE)

			val selector = s"(${userAttribute}=${id})"
			val extraQuery = ldapCF.extraUsers.map(_.mkString("(",")(",")"))
			val extraSelector = extraQuery.map(p => s"(&($selector)(|$p))")

			val n: NamingEnumeration[SearchResult] = ctx.search(baseDN, selector, ctrls)
			if (n.hasMore) {
				val user = n.next
				try {
					checkLdapPassword(url, user.getNameInNamespace, new String(credential.asInstanceOf[PasswordCredential].getPassword))
					val roles = checkGroups(ctx, ldapCF.groups.get, user.getNameInNamespace)
					if (roles.isEmpty) {
						if ( !extraSelector.map(p => ctx.search(baseDN, p,ctrls).hasMore).getOrElse(false) )
							throw new Exception("no matching roles found")
					}
					Right(Right(createAccount(id, Seq("query"))))
				}
				catch {
					case x: Exception => 
						Right(Left(x.getMessage))
				}
			}
			else 
				Left("User not found in LDAP")
		}
		catch {
			case x: Throwable => {
				//					LOG.error("Failure CATCHED",x)
//				Left(x.getMessage)
				Right(Left(x.getMessage))
			}
		}
		finally {
			Option(ctx).foreach(_.close)
		}
	}

	private def checkLdapPassword(url: String, userDN: String, passwd: String) = {
		var ctx: InitialDirContext = null
		try {
			val env = new Hashtable[String, String]
			env.put(Context.PROVIDER_URL, url.trim)
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory")
			env.put(Context.SECURITY_AUTHENTICATION, "simple")
			env.put(Context.SECURITY_PRINCIPAL, userDN)
			env.put(Context.SECURITY_CREDENTIALS, passwd)
			ctx = new InitialDirContext(env)
		}
		finally Option(ctx).foreach(_.close)

	}
}
