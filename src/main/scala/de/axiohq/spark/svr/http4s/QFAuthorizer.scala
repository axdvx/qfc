/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.http4s

import io.undertow.security.idm.Account
import io.undertow.security.idm.Credential
import java.security.Principal
import scala.collection.JavaConverters._

import de.axiohq.spark.svr._
import de.axiohq.spark.svr.Logging
import zio.Runtime

case class BasicUserPrincipal(id: String) extends Principal {
	override def getName = id
}

protected trait QFAuthorizer extends Logging {
	def verify(id: String, credential: Credential)(implicit rt: Runtime[AppEnv]): Either[String, Either[String, Account]]
	def createAccount(id: String, groups: Seq[String]): Account =
		new Account {
			override def getRoles = groups.toSet.asJava
			override def getPrincipal = new BasicUserPrincipal(id)
		}
}
