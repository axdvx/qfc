/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package de.axiohq.spark.svr.http4s

import java.security.MessageDigest
import java.util.Base64
import scala.util.Random

object Passwords {
	def check(pwd: String, hash0: String): Boolean = {

		val algo = hash0.substring(2,5)
		val data = Base64.getDecoder.decode(hash0.substring(6))

		val digest = MessageDigest.getInstance(algo)

		hash0 equals hash(pwd, algo, data.slice(digest.getDigestLength, digest.getDigestLength+4))
	}

	def hash(pwd: String, algo: String = "SHA", salt0: Array[Byte] = null): String = {

		val digest = MessageDigest.getInstance(algo)

		val salt = Option(salt0).getOrElse {
			val _salt = Array.ofDim[Byte](4)
			new Random().nextBytes(_salt)
			_salt
		}

		digest.update(pwd.getBytes("UTF-8"))
		digest.update(salt)

		val result0 = digest.digest
		val algo1 = algo.toUpperCase

		s"{S${algo1}}" + Base64.getEncoder.encodeToString(result0++salt)
	}

	def main(args:Array[String]) = {
		val h0 = hash("test")
		println(h0)
		println(check("test1", h0))
	}
}
