/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.http4s


import de.axiohq.spark.svr._

import io.undertow.security.idm.Account
import io.undertow.security.idm.Credential
import io.undertow.security.idm.PasswordCredential
import scala.collection.JavaConverters._
import scala.util.Failure
import scala.util.Success
import scala.util.Try

import de.axiohq.spark.svr._
import de.axiohq.spark.svr.config._
import zio.Runtime

object LocalAuthorizer extends QFAuthorizer {

	override def verify(id: String, credential: Credential)(implicit rt: Runtime[AppEnv]): Either[String, Either[String, Account]] = {
		LOG.info("Authentication: running Local")
		Try {
			val usersCF: Seq[UserCF] = rt.unsafeRun[Throwable, Seq[UserCF]](Configuration.conf.map(_.users))

			val pcred = credential match {
				case credential: PasswordCredential => Some(new String(credential.getPassword))
				case _ => None
			}
			val result = for {
				ido <- Option(id).map(_.trim).find(_.nonEmpty)
				user <- usersCF.find(_.name.equalsIgnoreCase(ido))
				pwd <- pcred
			} yield (Passwords.check(pwd, user.password), user.roles)
			result match {
				case Some((true, groups)) => Right(Right(createAccount(id, groups)))
				case Some((false,_)) => Right(Left("Invalid Login"))
				case None => Left("No such local user")
			}
		} match {
			case Failure(x) => Right(Left(x.getMessage))
			case Success(result) => result
		}
	}
}
