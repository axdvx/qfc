/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.http4s

import de.axiohq.spark.svr.Logging
import java.io.FileInputStream
import java.net.InetSocketAddress
import java.security.KeyStore
import java.security.Principal
import java.util.Base64

import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.SSLContext
import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpSession}
import javax.servlet.{DispatcherType, Filter, Servlet}
import cats.effect.{Blocker, ConcurrentEffect, ContextShift, IO, Resource}
import io.chrisdavenport.vault.Key
import io.undertow.server.{HttpHandler, HttpServerExchange}
import io.undertow.server.handlers.{PathHandler, SSLHeaderHandler}
import io.undertow.servlet.Servlets
import io.undertow.servlet.api.{DeploymentInfo, InstanceFactory, ServletInfo, ThreadSetupAction, ThreadSetupHandler}
import io.undertow.servlet.handlers.ServletRequestContext
import io.undertow.servlet.util.ImmediateInstanceHandle
import io.undertow.{Handlers, Undertow, UndertowOptions}
import org.http4s.server.{Server, ServerBuilder, _}
import org.http4s.servlet.{AsyncHttp4sServlet, BlockingServletIo, ServletContainer, ServletIo}
import org.http4s.{HttpApp, ParseResult, Request}

import scala.concurrent.duration._

case class SSLConfig(socketAddress: InetSocketAddress, keystore: String, passwd: String, keyname: String)

class UndertowBuilder[F[_]] private (
	socketAddress: Option[InetSocketAddress],
	sslConfig: Option[SSLConfig],
	mounts: Vector[Deploy[F]],
	handlers: Vector[Deploy[F]],
	deployments: Vector[Deploy[F]],
	banner: Seq[String],
	private val serviceErrorHandler: ServiceErrorHandler[F],
	private val servletIo: ServletIo[F],
	private val asyncTimeout: Duration
)(implicit protected val F: ConcurrentEffect[F])
	extends ServletContainer[F]
	with ServerBuilder[F]
	with Logging {
	type Self = UndertowBuilder[F]

	private def copy(
		socketAddress: Option[InetSocketAddress] = socketAddress,
		sslConfig: Option[SSLConfig] = sslConfig,
		mounts: Vector[Deploy[F]] = mounts,
		handlers: Vector[Deploy[F]] = handlers,
		deployments: Vector[Deploy[F]] = deployments,
		banner: Seq[String] = banner,
		serviceErrorHandler: ServiceErrorHandler[F] = serviceErrorHandler,
		servletIo: ServletIo[F] = servletIo,
		asyncTimeout: Duration = asyncTimeout
	): Self = new UndertowBuilder(socketAddress, sslConfig, mounts, handlers, deployments,banner, serviceErrorHandler, servletIo, asyncTimeout)

	override def withBanner(banner: scala.collection.immutable.Seq[String]): Self = copy(banner = banner)

	override def withServiceErrorHandler(serviceErrorHandler: ServiceErrorHandler[F]): Self = copy(serviceErrorHandler = serviceErrorHandler)

	override def withServletIo(servletIo: ServletIo[F]): Self = copy(servletIo = servletIo)

	private[this] def deployServlet(
		root: PathHandler,
		container: io.undertow.servlet.api.ServletContainer,
		prefix: String,
		servletInfo: ServletInfo,
		name: String,
			extension: Option[DeploymentInfo => DeploymentInfo]
	): Unit = {
		val di = new DeploymentInfo
		di.setClassLoader(this.getClass.getClassLoader)
		di.setContextPath(prefix)
		di.setDeploymentName(name)
		if(servletInfo != null) di.addServlet(servletInfo.addMapping("/*"))
		extension.foreach(_(di))

		val manager = container.addDeployment(di)
		manager.deploy
		val pathHandler = manager.start
		root.addPrefixPath(di.getContextPath, pathHandler)
	}
	def addDeployment(di: => QFDeployment, services: Map[String, HttpApp[F]] = Map.empty): Self = {
		copy(deployments = deployments :+ Deploy[F] {(root, container, index, builder) => 
			val di1 = di
			for { (path, route: HttpApp[F]) <- services } {
				val servlet =new AsyncHttp4sServlet(
					service = route,
					asyncTimeout = builder.asyncTimeout,
					servletIo = builder.servletIo,
					serviceErrorHandler = builder.serviceErrorHandler) {
					override def toRequest(req: HttpServletRequest): ParseResult[Request[F]] = {
						Option(req.getAttribute("javax.servlet.request.ssl_session_id")).foreach {
							case x: Array[Byte] => req.setAttribute("javax.servlet.request.ssl_session_id", UndertowBuilder.enc64(x))
						}
						super.toRequest(req)
							.map(_.withAttribute(UndertowBuilder.utsKey, Option(req.getUserPrincipal)))
							.map(_.withAttribute(UndertowBuilder.sesKey, Option(req.getSession)))
					}
				}
				di1.addServlet(servlet, path)
			}

			val manager = container.addDeployment(di1)
			manager.deploy
			val pathHandler = manager.start
			root.addPrefixPath(di1 getContextPath, pathHandler)
		})
	}

	override def mountServlet(servlet: HttpServlet, prefix: String, name: Option[String] = None): Self = this

	override def mountFilter(
		filter: Filter,
		urlMapping: String,
		name: Option[String],
		dispatches: java.util.EnumSet[DispatcherType]
	): Self = copy(mounts = mounts :+ Deploy[F] { (root, container, index, _) => Unit})

	def mountService(service: HttpApp[F], prefix: String, extension: DeploymentInfo => DeploymentInfo = null): Self =
		copy(mounts = mounts :+ Deploy[F] { (root, container, index, builder) =>
			val servlet = new AsyncHttp4sServlet(
				service = service,
				asyncTimeout = builder.asyncTimeout,
				servletIo = builder.servletIo,
				serviceErrorHandler = builder.serviceErrorHandler
			)

			val servletInfo = Servlets.servlet(s"servlet-${index}", servlet.getClass,
				new InstanceFactory[Servlet]() {
					def createInstance = new ImmediateInstanceHandle(servlet)
				}
			)
			servletInfo.setAsyncSupported(true)

			deployServlet(root, container, prefix, servletInfo, s"servlets-${index}", Option(extension))
		})

	def mountHandler(handler: HttpHandler, path: String): Self =
		copy(handlers = handlers :+ Deploy[F] { (root, container, index, builder) =>
			val pathHandler = Handlers.path().addExactPath(path, handler);
		})

	def bindHttps(port: Int, host: String, keystore: String, ksPasswd: String, ksName: String): Self = 
		copy(
			sslConfig = Some(SSLConfig(new InetSocketAddress(host, port), keystore, ksPasswd, ksName))
		)
	
	override def bindSocketAddress(socketAddress: InetSocketAddress): Self =
		copy(socketAddress = Option(socketAddress))

	def resource: Resource[F, Server[F]] =
		Resource(F.delay {
				
			def mkSSLContext(sslConfig: SSLConfig): SSLContext = {
				var fi: FileInputStream = null
				try {
					fi = new FileInputStream(sslConfig.keystore)
					val ks = KeyStore.getInstance("PKCS12");
					ks.load(fi, sslConfig.passwd.toCharArray)
					val kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm);
					kmf.init(ks, sslConfig.passwd.toCharArray)
					val kms = kmf.getKeyManagers
					val ctx = SSLContext.getInstance("TLS")
					ctx.init(kms, null, null)
					ctx
				}
				finally if (fi != null) fi.close
			}	
				
			val root = new PathHandler

			val container = Servlets.defaultContainer
			for ((deploy, i) <- deployments.zipWithIndex) deploy.f(root, container, i, this)
			
			val undertow = for {
				b0 <-	Option(Undertow.builder)
				b1 =	b0.setServerOption(UndertowOptions.ENABLE_HTTP2, java.lang.Boolean.TRUE)
				b2 <-	socketAddress.map(a => b1.addHttpListener(a.getPort, a.getHostString)).orElse(Option(b1))
				b3 <-	sslConfig.map(c => b2.addHttpsListener(c.socketAddress.getPort, c.socketAddress.getHostString, mkSSLContext(c))).orElse(Option(b2))
				b4 =	b3.setHandler(root)
			} yield (b4.build)
				
			undertow.foreach { u =>
				u.start
				LOG.info(banner.mkString("", "\n", ""))
				LOG.info(s"Server ready. Listening:")
				socketAddress.foreach(p => LOG.info(s"  http://${p.getHostName}:${p.getPort}"))
				sslConfig.foreach(p => LOG.info(s"  https://${p.socketAddress.getHostName}:${p.socketAddress.getPort}"))
			}
			
			val server = new Server[F] {
				val isSecure = false
				lazy val address: InetSocketAddress = socketAddress.orElse(sslConfig.map(_.socketAddress))
					.getOrElse(InetSocketAddress.createUnresolved("", 0))
			}

			server -> F.delay {undertow.foreach(_.stop)}
		})
}

object UndertowBuilder {
	val utsKey = Key.newKey[IO,Option[Principal]].unsafeRunSync
	val sesKey = Key.newKey[IO,Option[HttpSession]].unsafeRunSync

	def enc64(data: Array[Byte]) = Base64.getEncoder.encodeToString(data)
	
	def apply[F[_]: ConcurrentEffect](implicit blocker: Blocker, cs: ContextShift[F]) = new UndertowBuilder[F](
		socketAddress = None,
		sslConfig = None,
		mounts = Vector.empty,
		handlers = Vector.empty,
		deployments = Vector.empty,
		banner = defaults.Banner,
		//		asyncTimeout = defaults.ResponseTimeout,
		asyncTimeout = 10 minutes,
		serviceErrorHandler = DefaultServiceErrorHandler,
		servletIo = BlockingServletIo[F](8192, blocker)
	)
}

private final case class Deploy[F[_]](f: (PathHandler, io.undertow.servlet.api.ServletContainer, Int, UndertowBuilder[F]) => Unit)
