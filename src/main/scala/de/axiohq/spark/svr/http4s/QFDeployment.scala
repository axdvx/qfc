/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.http4s

import de.axiohq.spark.svr._
import de.axiohq.spark.svr.model.RepoServlet
import io.undertow.security.idm.Account
import io.undertow.security.idm.Credential
import io.undertow.security.idm.IdentityManager
import io.undertow.server.handlers.resource.ClassPathResourceManager
import io.undertow.servlet.Servlets
import io.undertow.servlet.api._
import io.undertow.servlet.util.ImmediateInstanceHandle
import javax.servlet.http.{HttpServlet, HttpServletRequest}
import de.axiohq.spark.svr.config._

class QFDeployment extends DeploymentInfo {
	def addServlet(servlet: HttpServlet, path: String): QFDeployment = {
		val name = path.stripMargin('/').replaceAll("/", "-")

		val servletInfo = Servlets.servlet(s"${name}-servlet", servlet.getClass,
			new InstanceFactory[HttpServlet] {
				def createInstance = new ImmediateInstanceHandle(servlet)
			})
		servletInfo.setAsyncSupported(true)
		servletInfo.addMapping(path)
		addServlet(servletInfo)
		this
	}
}

object QFDeployment extends Logging {

	def Main(implicit rt: zio.Runtime[AppEnv]): QFDeployment = {
		val cfg: CF = rt.unsafeRun(Configuration.conf)
		val idm = chainedIdentityManager
		val di = new QFDeployment
		di.setDeploymentName("QF-Server")
		di.setResourceManager(new ClassPathResourceManager(this.getClass.getClassLoader, "dist"))

		di.addWelcomePage("index.html")

		di.addServlet(new AuthServlet, "/auth")
		di.addServlet(new FormServlet, "/form")

		di.setContextPath("/")
		di.setClassLoader(di.getClass.getClassLoader)
		if (cfg.server.secured.getOrElse(true)) secure(di, idm)
		else LOG.warn("No Security on /")
		di
	}

	def chainedIdentityManager(implicit rt: zio.Runtime[AppEnv]) = new IdentityManager {

		override def verify(credential: Credential): Account = null

		override def verify(account: Account): Account = account

		override def verify(id: String, credential: Credential): Account = {
			Seq(LocalAuthorizer, LdapAuthorizer).iterator
				.map(_.verify(id, credential))
				.find(_.isRight) match {
					case Some(Right(Right(account))) => account
					case _ => null
				}
		}
	}

	def Repo(implicit rt: zio.Runtime[AppEnv]): QFDeployment = {
		val cfg: CF = rt.unsafeRun(Configuration.conf)
		val idm = chainedIdentityManager

		val di = new QFDeployment
		di.setDeploymentName("QF-Repos")
		di.addServlet(new RepoServlet, "/*")
		di.setContextPath("/repo")
		di.setClassLoader(di.getClass.getClassLoader)

		if (cfg.server.secured.getOrElse(true)) secure(di, idm, true)
		else LOG.warn("No Security on /repo")
		di
	}

	private def secure(di: DeploymentInfo, idm: IdentityManager, basic: Boolean = false): DeploymentInfo = {
		//		di setAuthenticationMode(AuthenticationMode.CONSTRAINT_DRIVEN)
		val lc = new LoginConfig("Spark Realm", "/form", "/form")
		if (!basic) lc.addFirstAuthMethod(HttpServletRequest.FORM_AUTH)
		lc.addLastAuthMethod(HttpServletRequest.BASIC_AUTH)
		di setIdentityManager (idm)
		di setLoginConfig (lc)
		val constraint = new SecurityConstraint
		val resources = new WebResourceCollection
		basic match {
			case true => resources.addUrlPatterns("/*")
			case _ => resources.addUrlPatterns("/api/*", "/index.html", "/js/*", "/css/*")
		}
		constraint.addWebResourceCollection(resources)
		constraint.addRoleAllowed("query")
		constraint.setEmptyRoleSemantic(SecurityInfo.EmptyRoleSemantic.DENY)
		di addSecurityConstraint constraint
	}

}
