/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.http4s.encoders
import de.axiohq.spark.svr.api._
import io.circe.{Encoder, Json}
import io.circe.syntax._
import io.circe.generic.semiauto._
import org.apache.spark.sql.Row
import org.apache.spark.sql.catalyst.plans.logical.Histogram
import org.apache.spark.sql.types._

package object sparkEncoders {
	implicit val encodeString = Encoder.encodeString.contramap[String](Option(_).getOrElse(""))

	implicit val exceptionEncoder: Encoder[Throwable] = new Encoder[Throwable] {
		def apply(x: Throwable): Json = Json.obj(
			("msg", x.getMessage.asJson), ("type", x.getClass.getSimpleName.asJson)
		)
	}
	implicit val structFieldEncoder : Encoder[StructField] = new Encoder[StructField] {
		def apply(sf: StructField): Json = Json.obj(
			("name", sf.name.asJson), ("type", sf.dataType.typeName.asJson)
		)
	}

	val rowEncoder1: Encoder[Row] = new Encoder[Row] {
		def apply(row: Row): Json = row.schema.fields.zipWithIndex
			.map({ case (a, i) => row.get(i) match { case null => null case x: Any => x.toString } })
			.asJson
	}
	
	implicit val histogramEncoder : Encoder[Histogram] = new Encoder[Histogram] {
		def apply(histogram: Histogram): Json = Json.obj(
			("height", histogram.height.asJson), ("densities", densities(histogram).asJson)
		)
	}
	
	private[this] def densities(histogram: Histogram)= histogram.bins
			.groupBy(_.lo)
			.map(p => (p._1.longValue,p._2.map(_.ndv).reduce(_+_)*histogram.height))
	
	val mappers = (r: Row) => (i: Int) => {(dt: DataType) => dt match {
		case StringType => r.getString(i)
		case DoubleType => r.getDouble(i)	
		case FloatType => r.getFloat(i)
		case LongType => r.getLong(i)
		case IntegerType => r.getInt(i)
		case ShortType => r.getShort(i)
		case DateType => r.getDate(i)
		case TimestampType => r.getTimestamp(i)
		case DecimalType() => r.getDecimal(i) match {case null => null case x => x.doubleValue}
		case NullType => null
	}}

	implicit val rowEncoder: Encoder[Row] = new Encoder[Row] {
		final def apply(row: Row): Json = {
			def mapper: Seq[Json] = row.schema.fields.map(_.dataType).zipWithIndex.map(r => 
				r match {
					case (DataTypes.StringType, i) => Option(row.getString(i)).map(_.asJson).orNull
					case (DataTypes.ShortType, i) => Option(row.getShort(i)).map(_.asJson).orNull
					case (DataTypes.NullType, i) => null
					case (DataTypes.LongType, i) => Option(row.getLong(i)).map(_.asJson).orNull.asJson
					case (DataTypes.IntegerType, i) => Option(row.getInt(i)).map(_.asJson).orNull.asJson
					case (DataTypes.FloatType, i) => Option(row.getFloat(i)).map(_.asJson).orNull.asJson
					case (DataTypes.DoubleType, i) => Option(row.getDouble(i)).map(_.asJson).orNull.asJson
					case (DataTypes.DateType, i) => Option(row.getDate(i)).map(_.toLocalDate.toString.asJson).orNull
					case _ => s"<${r}>".asJson
			}).toSeq

			Json.arr(mapper: _*)
		}
	}


}






