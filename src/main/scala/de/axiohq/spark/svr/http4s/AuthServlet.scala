/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.http4s

import de.axiohq.spark.svr.Logging
import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}

import scala.util.{Failure, Success, Try}

class AuthServlet extends HttpServlet with Logging {
	override def doPost(req: HttpServletRequest, res: HttpServletResponse): Unit = {
		val user = req.getParameter("j_username")
		val passwd = req.getParameter("j_password")

		val result = Try(req.login(user, passwd)) match {
			case Success(_) => (req.getHeader("referer"), HttpServletResponse.SC_SEE_OTHER)
			case Failure(_) => (req.getRequestURI, HttpServletResponse.SC_UNAUTHORIZED)
		}
		res.setStatus(result._2)
		res.sendRedirect(result._1)
	}

	override def doGet(req: HttpServletRequest, res: HttpServletResponse): Unit = {
		req.logout
		req.getSession.invalidate
		res.setStatus(HttpServletResponse.SC_SEE_OTHER)
		res.sendRedirect(req.getContextPath)
	}
}
