/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr.api

import org.apache.spark.sql.catalyst.TableIdentifier
import org.apache.spark.sql.catalyst.catalog.CatalogColumnStat
import org.apache.spark.sql.types.StructField

case class SchemaInfo(name: String, location: String)

case class TableInfo(
	identifier: TableIdentifier,
	tableType: String,
	format: Option[String],
	provider: Option[String],
	createVersion: String,
	owner: String,
	createTime: String,
	ddlTime: Option[String],
	stats: Option[TableStats],
	columnInfos: Array[ColumnInfo])

case class TableStats(
	rowCount: Option[BigInt],
	sizeInBytes: BigInt)

case class ColumnInfo(
	field: StructField,
	partition: Boolean,
	stats: Option[CatalogColumnStat])
