
const prod = process.env.NODE_ENV === 'production'
module.exports = {
  presets: [
    "@vue/cli-plugin-babel/preset"
  ],  
  plugins: prod ? ["transform-remove-console"] : []

}


