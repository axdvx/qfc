module.exports = {
	future: {
		removeDeprecatedGapUtilities: true,
		purgeLayersByDefault: true
	},
//	purge: ['./index.html', './src/**/*.{vue,js}'],
	purge: false,
	theme: {
		extend: {},
	},
	variants: {
		backgroundColor: ['responsive', 'hover', 'focus', 'active'],
		textColor: ['responsive', 'hover', 'focus', 'active'],
	},
	plugins: [],
}
