
module.exports = {
	publicPath: "./",
	outputDir: "../resources/dist/",
	productionSourceMap: false,
	devServer: {
		proxy: {"/api": {target: "http://localhost:8580/"}},
		watchOptions: {poll: false}
	},
	chainWebpack: config => {
		config.module.rule("vue")
				.use("vue-svg-inline-loader")
				.loader("vue-svg-inline-loader")
				.options({});
	},
}
