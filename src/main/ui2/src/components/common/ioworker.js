/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import ExcelJS from "exceljs";
import ArrowTypes from "./ArrowTypes";
import {Table} from "apache-arrow";
import { writeToBuffer } from '@fast-csv/format';

onmessage = function (event) {
	const table = Table.from([event.data[0]])
	const xlsx = event.data[1]
	const rowid = event.data[2]

	const ids = table.schema.fields.map(p => p.typeId)
	const fmts = ids.map(p => ArrowTypes[p])
	const cols = table.schema.fields.map(p => table.getColumn(p.name))

	const columns = table.schema.fields.map(p => ({header: p.name, id: p.name}))
	var n = 0
	var batch = 0
	const rows = []
	if (!xlsx||rowid > -1) rows.push(table.schema.fields.map(p=>p.name))
	while (n < table.length) {
		if (rowid > -1 && n!==rowid) {
			n += 1
			batch += 1
			continue
		}
		const row = cols.map((col, i) => fmts[i].csvFmt(col.field, col.get(n)))
		rows.push(row)
		n += 1
		batch += 1
		if (batch > table.length/10) {
			postMessage([false, Math.floor(n/table.length*100,0)])
			batch = 0	
		}
	}
	postMessage([false, 95])
	const transpose = p => Object.keys(p[0]).map(cn => p.map(rn => rn[cn]))
	if (xlsx) {
		var workbook = new ExcelJS.Workbook()
		const sheet = workbook.addWorksheet('Result');
		if (rowid > -1) {
			sheet.columns = ["COLUMN", "VALUE"].map(p => ({header: p, id: p, style: {alignment: {horizontal: "left"}}}))
			sheet.addRows(transpose(rows))
		}
		else {
			sheet.columns = columns
			sheet.addRows(rows)
		}
		workbook.xlsx.writeBuffer()
				.then(p => postMessage([true, new Blob([p]),{type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}]))
	}
	else {
		writeToBuffer(rowid > -1 ?transpose(rows) : rows).then(p => postMessage([true, new Blob([p]),{type: "text/csv;charset=utf-8"}])) 
	}
}


