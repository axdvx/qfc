/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {Type} from "apache-arrow";

class BaseType {
	static NULL = "<null>"
	cellFmt(field, o) {
		return o && o.value ? o.value : BaseType.NULL
	}
	asNative(field,value) {
		return value
	}
	csvFmt(field,value) {
		return value ? this.asNative(field,value) : null 
	}
	isNum() {
		return true
	}
}

class BigDecimal extends BaseType {
	static LANG = "en-US"
	static fmt = v => Intl.NumberFormat(BigDecimal.LANG, {useGrouping: true}).format(v)
	static decimalChar = Intl.NumberFormat(BigDecimal.LANG, {useGrouping: true}).format(0.1)[1]
	
	asNative(field, v) {
		return this.cellFmt(field, v, true)
	}
	cellFmt(field, obj, native=false) {
		if (native && !obj) return 0
		if (!native && (!obj||(!obj.value))) return BaseType.NULL
		const buffer = native ? obj : obj.value
		//refactor
		const scale = field.type.scale
		if (!buffer || typeof (buffer) === "string")
			return buffer

		const _v = buffer
		const neg = _v[3] & 0x80000000
		
		const v = (neg)
				? Uint32Array.from([~(_v[3]), ~(_v[2]), ~(_v[1]), ~(_v[0])])
				: Uint32Array.from([_v[3], _v[2], _v[1], _v[0]])
		const tohex = (p) => p.toString(16).padStart(8, "0")

		const ar = tohex(v[0]) + tohex(v[1]) + tohex(v[2]) + tohex(v[3])

		const abs = neg ? BigInt("0x" + ar) + BigInt(1) : BigInt("0x" + ar)
		const str = abs.toString()

		const pad = scale - str.length + 1
		const str1 = str.padStart(scale + 1, "0")
		
		const fmt = (native) ? p=>p : BigDecimal.fmt
		const str2 = (scale) ? fmt(str1.slice(0, -scale)) + BigDecimal.decimalChar + str1.slice(-scale)
							: fmt(str1)
		const r = (neg ? "-" : "") + str2
		return (native) ? parseFloat(r) : r
	}
}

class NumberType extends BaseType {
	cellFmt(field, o) {
		console.log("NumberType", )
		return o && o.value !== null ? Intl.NumberFormat("en-US", {useGrouping: true,maximumSignificantDigits:16}).format(o.value) : BaseType.NULL
	}
	asNative(field, o) {
		return o ? o.valueOf() : null
	}
}

class IntType extends BaseType {
	cellFmt(field, o) {
		return o && o.value !== null ? Intl.NumberFormat("en-US", {useGrouping: true}).format(o.value) : BaseType.NULL
	}
	asNative(field, o) {
		return o ? o.valueOf() : null
	}
}

class FloatType extends BaseType {
	cellFmt(field, o) {
		const nd = field.type.precision === 1 ? 8 : 17
		return o && o.value !== null ? Intl.NumberFormat("en-US", {useGrouping: true,maximumSignificantDigits:nd}).format(o.value) : BaseType.NULL
	}
	asNative(field, o) {
		return o ? o.valueOf() : null
	}
}

class DateType extends BaseType {
	isNum() {
		return false
	}
	cellFmt(field, o) {
		return o && o.value && o.value.toISOString ? o.value.toISOString().slice(0, 10) : BaseType.NULL
	}
}
class TimestampType extends BaseType {
	isNum() {
		return false
	}
	dtf = Intl.DateTimeFormat("sv-SE", {
		year: "numeric", month: "numeric", day: "numeric",
		hour: "numeric", minute: "numeric", second: "numeric", hour12: false
	})
	cellFmt(field, o) {
		if (typeof o.value === "string") return o.value
//		return o && o.value ? new Date(o.value).toISOString().replace(/T/, " ").replace(/\..*$/,"") : BaseType.NULL
		return o && o.value ? this.dtf.format(o.value) : BaseType.NULL
	}
	csvFmt(field, o) {
		return this.cellFmt(field, {value:o})
	}
	
}
class Utf8Type extends BaseType {
	isNum() {
		return false
	}
	csvFmt(field, o) {
		if (o==null) return o
		if (o.indexOf("\"") < 0 && o.indexOf(",") < 0 ) return o
		return "\""+o.replace(/"/g, "\"\"")+"\""
	}
}

class ArrowTypes {
	static [Type.Decimal] = new BigDecimal
	static [Type.Int] = new IntType
	static [Type.Float] = new FloatType
	static [Type.Date] = new DateType
	static [Type.Timestamp] = new TimestampType
	static [Type.Utf8] = new Utf8Type
}

//export default ArrowType
export default new Proxy(ArrowTypes, {
	get: (o, k) => k in o ? o[k] : new BaseType
})

