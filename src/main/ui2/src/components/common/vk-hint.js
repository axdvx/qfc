/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css';
import "./vk-hint.css";

tippy.setDefaultProps({delay: [1000, 10], placement: "bottom-start"})

export default {
	bind(el, binding, vnode) {
		if (vnode.data.attrs && vnode.data.attrs.hint) {
			const opts = {content: vnode.data.attrs.hint}
			if (vnode.data.attrs["hint-pos"]) opts.placement = vnode.data.attrs["hint-pos"]
			tippy(el, opts)
		}
	},
	componentUpdated(el, binding, vnode, oldvnode) {
		if (el._tippy && vnode.data.attrs) {
			if ( vnode.data.attrs.hint) el._tippy.setContent(vnode.data.attrs.hint)
			const pos =  vnode.data.attrs["hint-pos"]
			const props = {placement: pos ? pos : tippy.defaultProps.placement}
			el._tippy.setProps(props)
		}
	},
	unbind(el, binding, vnode) {
		if (el._tippy) el._tippy.destroy()
	}
}

