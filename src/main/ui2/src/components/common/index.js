/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import splitpane from "./splitpane";
import VkButton from "./vk-button";
import VkTabview from "./vk-tabview";
import VkTabpane from "./vk-tabpane";
import VkSelect from "./vk-select";
import VkModal from "./vk-modal";
import VkInput from "./vk-input";
import VkSvgTi from "./vk-svg-ti";
import VkScroll from "./vk-scroll";
import VkCodemirror from "./vk-codemirror";
import VkChooser from "./vk-chooser";
import VkHint from "./vk-hint";

class Vk {
	install(Vue, options) {
		Vue.component("splitpane", splitpane)
		Vue.component("vk-button", VkButton)
		Vue.component("vk-tabview", VkTabview)
		Vue.component("vk-tabpane", VkTabpane)
		Vue.component("vk-select", VkSelect)
		Vue.component("vk-modal", VkModal)
		Vue.component("vk-input", VkInput)
		Vue.component("vk-svg-ti", VkSvgTi)
		Vue.component("vk-codemirror", VkCodemirror)
		Vue.component("vk-chooser", VkChooser)
		Vue.directive('scroll', VkScroll)
		Vue.directive("hint", VkHint);
	}
}
export default new Vk
