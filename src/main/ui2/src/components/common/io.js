/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import ArrowTypes from "./ArrowTypes";
import FileSaver from "file-saver";
import IoWorker from 'worker-loader!./ioworker.js';

import ExcelJS from "exceljs";
import { writeToBuffer } from '@fast-csv/format';

class io {
	MIME_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
	MIME_CSV = "text/csv;charset=utf-8"

	exportData(data, xlsx = false) {
		if (!data) return 0
		
		const fmt = p => ArrowTypes[data.measure.type.typeId].csvFmt(data.measure, p)
		
		const headers = data.cols.map(p => data.rpivots.map(r=>"").concat(p))
		const body = data.rows.map(p => p.row.concat(p.data.map(fmt)))
		const tbl = headers.concat(body)
		
		if (xlsx) {
			var workbook = new ExcelJS.Workbook()
			const sheet = workbook.addWorksheet('Result');
			sheet.addRows(tbl)
			workbook.xlsx.writeBuffer()
					.then(p => FileSaver.saveAs(new Blob([p], {type: this.MIME_XLSX}), "pivot.xlsx"))
		}
		else
			writeToBuffer(tbl).then(p => FileSaver.saveAs(new Blob([p], {type: this.MIME_CSV}), "pivot.csv"))
	}

	exportArrow(table, xlsx = true, rowid, complete = _ => {}, progress = _ => {}) {
		const w = new IoWorker
		w.postMessage([table, xlsx, rowid])
		w.onmessage = z => {
			if (!z.data[0]) {
				progress(z.data[1])
				return 0
			}
			complete()
			if (xlsx) FileSaver.saveAs(z.data[1], "table.xlsx")
			else FileSaver.saveAs(z.data[1], "table.csv")
		}
	}
}

export default new io()
