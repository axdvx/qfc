/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import CodeMirror from "codemirror";
import "codemirror/lib/codemirror.css";
import "./vcm-0.css";
import "codemirror/mode/sql/sql";
import "codemirror/addon/hint/show-hint";
import "codemirror/addon/hint/show-hint.css";
import "codemirror/addon/search/search.js";
import "codemirror/addon/search/searchcursor.js";
import "codemirror/addon/search/jump-to-line.js";
import "codemirror/addon/selection/active-line.js";
import "codemirror/addon/dialog/dialog.js";
import "codemirror/addon/dialog/dialog.css";

import funcs from "./vcm-functions";

import jaro_winkler from "jaro-winkler";
import Worker from 'worker-loader!./vcm-worker.js';

import completer from "./vcm-completions";

export default {
	data: () => ({
			vmode: false,
			cm: null,
			worker: null,
			cmo: {
				cursorBlinkRate: 0,
				mode: "text/x-sparksql",
				theme: "default",
				lineNumbers: true,
				indentWithTabs: true,
				lineWiseCopyCut: false,
				smartIndent: false,
				scrollbarStyle: "native"
			},
			currentSql: null,
			syntax: null,
			message: null,
		}),
	props: {
		doc: Object,
		sqlMessage: Object,
		status: Object
	},
	computed: {
	},
	watch: {
		sqlMessage(o, n) {
			this.showMessage(o, n)
		},
		doc(n,o) {
			console.log("### doc-old", o?{fname:o.fname, discard:o.discard}:"NOTHING",)
			console.log("### doc-new", {fname:n.fname, discard:n.discard},)
			
			if (n.discard || !o) {
				this.cm.setValue(n.txt)
				this.cm.getDoc().clearHistory()
			}
			else 
				this.$svr.repoSave(o.fname , this.cm.getValue())
					.then(p => {this.cm.setValue(n.txt)})
		},
	},

	mounted() {
		const self = this
		this.worker = new Worker
		this.cmo.extraKeys = {
			"Ctrl-Space": "autocomplete",
			"Ctrl-Enter": this.run,
			"Ctrl-S": () => this.onSave(),
			"Esc": this.clearMessage
		}
		this.cm = CodeMirror.fromTextArea(this.$refs.vcm, this.cmo)
		CodeMirror.commands.autocomplete = function (cm) {
			CodeMirror.showHint(cm, (cm, opt) =>completer.run(cm, self.currentSql.sql, opt))
		}

		this.cm.on("cursorActivity", this.onCursorActivity)
		this.cm.on("gutterClick", this.onGutterClick)
		this.cm.setOption("styleActiveLine", {})
		if (this.doc && this.doc.text) this.cm.setValue(this.doc.txt)
		else this.cm.setValue("\n--- no document\n")
		this.$bus.$on("save-doc", this.onSave)
//		window.addEventListener("beforeunload", () => this.value ? this.saveDocument(): {})
	},

	beforeDestroy() {
		this.$bus.$off("save-doc", this.onSave)
		this.cm.off("cursorActivity", this.onCursorActivity)
		this.cm.off("gutterClick", this.onGutterClick)
		this.worker.terminate()
	},
	updated() {
	},

	methods: {
		onSave(msg) {
			console.log("onsave",msg)
			return this.doc
					? this.$svr.repoSave(this.doc.fname ,this.cm.getValue(), msg)
					: Promise.resolve(0)
		},
		
		clearMessage() {
			const msg = this.$refs.message
			msg.style.display = "none"
		},
		onCursorActivity(cm) {
			this.syntax = null
			
			
			this.clearMessage()
			this.currentSql = funcs.segments(cm)
			const self = this
			this.worker.onmessage = val => {
				const errors = val.data
				this.syntax = errors
				const lineClass = errors ? "current-sql-error" : "current-sql"
				for (var i = self.currentSql.line; i < self.currentSql.end + 1; ++i)
					cm.addLineClass(i, "gutter", lineClass)
			}
			if (self.currentSql && self.currentSql.sql) this.worker.postMessage(self.currentSql.sql)
		},
		run() {
			this.$emit("exec", this.currentSql.sql)
		},
		onGutterClick(cm, line1, gutter, event) {
			if (!this.syntax) return 0
			if (this.currentSql.line > line1 || this.currentSql.end < line1) return 0
			
			const rc = this.syntax
			const ch = rc.col ? rc.col : 0
			const line = (rc.line ? rc.line : 1) + this.currentSql.line - 1
			const msg = this.$refs.message
			
			this.message = rc.msg

			msg.style.display = "flex"
			this.cm.addWidget({line: line, ch: ch}, msg, true)
		},
		showMessage(o, n) {
			this.clearMessage();
			if (!this.sqlMessage) return 0 
			
			const rc = this.sqlMessage
			this.message = rc.message
			const msg = this.$refs.message
			
			const z = /cannot resolve '`(.*)`' given input columns: *\[(.*)\]/.exec(this.message)
			
			if (z && z.length > 2) {
				const word = z[1]
				const alternatives = z[2].split(",").map(p=>p.split(".").reverse()[0])
				const distf = p => [jaro_winkler(word,p, {caseSensitive: false}),p]
				const dists = alternatives.map(distf).sort((a,b)=>(a[0]<b[0]))
				const hints = dists.map(p=>p)
				this.message = "Maybe be one of:\n\n\  "+dists.slice(0,3).map(p=>p[1]).join("\t\n") 
					+ "\n\n" +this.message
			}

			const ch = rc.pos ? rc.pos : 0
			const line = (rc.line ? rc.line : 1) + this.currentSql.line - 1

			msg.style.display = "flex"
			this.cm.addWidget({line: line, ch: ch}, msg, true)
		}
	}
}
