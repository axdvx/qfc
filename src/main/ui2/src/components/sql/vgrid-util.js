/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import hljs from "highlight.js";
import "highlight.js/styles/default.css";


export default class VgridUtil {
	
	static kw = `Join Sort BroadcastExchange ReusedExchange ColumnarToRow
				Aggregate UnresolvedRelation SubqueryAlias HashAggregate
				Relation Project Filter Expand Exchange Hash Scan parquet
				Aggregate BroadcastHashJoin FileScan`.replace(/[\t\n]/g, " ")
	
	static f = () => 
					({case_insensitive: true, keywords: this.kw, 
						contains: [hljs.COMMENT('==', '$'), {className: 'fade', begin: '#', end: '[ ,]'}]
					})
	
	static init() {
		if (!hljs.getLanguage("plan"))
			hljs.registerLanguage("plan", this.f)
	}	
	static format(block) {
		this.init()
		return  hljs.highlightBlock(block)
	}				
}
