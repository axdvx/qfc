/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

export default new class {
	segments(cm)  {
		const doc = cm.getDoc();
		const segs = []
		const pos = doc.getCursor()
		var text = false
		var start = 0
		doc.eachLine(lh => {
			const ln = lh.lineNo()
			cm.removeLineClass(ln, "gutter", "current-sql")
			cm.removeLineClass(ln, "gutter", "current-sql-error")
			if (lh.text.trim().length === 0) {
				if (text) segs.push([start, ln - 1])
				text = false
			} else {
				if (!text) start = ln
				text = true
			}
			if (lh.text.trim().endsWith(";")) {
				if (text) segs.push([start, ln])
				text = false
			}
		})
		segs.push([start, doc.lastLine()])

		const seg = segs.filter(p => p[0] <= pos.line).filter(p => p[1] >= pos.line)[0]
		var offset = 0
		if (seg)
			doc.eachLine(lh => {
				const ln = lh.lineNo()
				if (ln >= seg[0] && ln <= seg[1]) {
//					cm.addLineClass(ln, "gutter", "current-sql")
					if (ln < pos.line) offset += lh.text.length + 1 // eol  
					else if (ln === pos.line) offset += pos.ch
				}
			})

		const cur = {sql: "", offset: offset}
		if (seg) {
			cur.sql = this.cleanSql(doc.getRange({line: seg[0], ch: 0}, {line: seg[1] + 1, ch: 0}))
			cur.line = seg[0]
			cur.end = seg[1]
		}
		return cur
	}
	cleanSql(sql) {
		var start = 0 
		while (sql[start] === " "||sql[start] === "\t") ++start
		var end = sql.length - 1
		while (sql[end] === " "||sql[end] === "\t"||sql[end] === ";" ||sql[end] === "\n" ) --end
		return sql.substring(start, end+1)
	}

}