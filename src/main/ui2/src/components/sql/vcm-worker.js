/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import antlr4 from "antlr4";
import {SqlBaseLexer} from "./g4//SqlBaseLexer";
import {SqlBaseParser} from "./g4/SqlBaseParser";
import UpperCaseStream from "./UpperCaseStream";

class Util {
	static tid = 0
	
	static walk(node,indent="") {
			if (node.ruleIndex > -1);
			else if (node.symbol)
				if (node.children) node.children.map(p => this.walk(p, indent+"  "))
	}
	
	static check(sql) {
		if (!sql || sql.lenght < 3) return

		var chars = new UpperCaseStream(sql);
		const lexer = new SqlBaseLexer(chars)
		lexer.strictMode = false

		const tokens = new antlr4.CommonTokenStream(lexer)
		const parser = new SqlBaseParser(tokens)

		parser.buildParseTrees = true
		var pr = null
		
		class ErrorListener extends antlr4.error.ErrorListener {
			syntaxError(recognizer, symbol, line, column, message, payload) {
				pr = {line:line, col:column, msg:message}
			}
		}
		parser.removeErrorListeners()
		parser.addErrorListener(new ErrorListener())
		const tree = parser.singleStatement()
		return tree.parser._syntaxErrors ? pr : null
	}
}

onmessage = function (event) {
	clearTimeout(Util.tid)
	Util.tid = setTimeout(_ => postMessage(Util.check(event.data)), 100)
}

