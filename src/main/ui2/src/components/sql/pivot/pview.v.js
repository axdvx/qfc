/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import ResizeSensor from "css-element-queries/src/ResizeSensor";
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';

import ArrowTypes from "@/components/common/ArrowTypes";
import cmaps from "./cmaps";
import Vue from "vue";
import FileSaver from "file-saver";
import DtIcon from "@/components/icons/dt-icon"

const state = Vue.observable({position: null})
export default {
	name: "pview",
	components: {DtIcon},
	data: () => ({
//			renderer: null,
//			scene: null,
//			camera: null,
//			controls: null,
//			raycaster: null,
//			font: null,
//			textMaterial: null,
			aid: null,
			rsensor: null,
			rendering: false,
			mouse: new THREE.Vector2(),
			mousebutton: false,
			sensor: false,
			pointer: {x: 0, y: 0},
			intersected: null,
//			datapoints: [],
			ncols: 0,
			nrows: 0,
			ctexts: [],
			rtexts: [],
			moving: false,
			sprite: null,
			tcache: {},
			label: {
				value: 0,
				cols: [],
				rows: []
			},
			platform: {c: null, u: null, d: null},
			
			fmtNative: p => p,
			fmtVisual: p => p
		}),
		
	props: {
		theme: {
			type: Array,
			default: () => cmaps.terrain
		},
		table: {
			type: Object,
			default: () => ({rows: [], cols: []})
		}
	},
	computed: {
		sensorClass() {
			return this.sensor?"active":""
		}
	},
	watch: {
		table(table1) {
			this.setup(table1)
		}
	},

	updated() {
	},
	
	mounted() {
		this.datapoints = []
		if (this.$refs.label) this.$refs.label.style.display = "none"
		const wdw = this.$refs.scene

		this.scene = new THREE.Scene()
		this.scene.background = new THREE.Color(0x808080)
		this.scene.fog = new THREE.FogExp2(0xcccccc, 0.002)

		this.renderer = new THREE.WebGLRenderer({antialias: true, preserveDrawingBuffer:true})
		this.renderer.setPixelRatio(window.devicePixelRatio)
		this.renderer.setSize(wdw.clientWidth, wdw.clientHeight)
		this.renderer.shadowMapEnabled = true
		this.renderer.shadowMap.type = THREE.PCFSoftShadowMap

		this.camera = new THREE.PerspectiveCamera(60, wdw.clientWidth / wdw.clientHeight, 1, 1000)
		this.camera.far = 1000000
		this.camera.updateProjectionMatrix()
		
		if (state.position)
			this.camera.position.set(state.position.x, state.position.y, state.position.z)
		else
			this.camera.position.set(-120, 60, 170)
		this.raycaster = new THREE.Raycaster()

		this.controls = new OrbitControls(this.camera, wdw)

		this.controls.enableDamping = true; 
		this.controls.enableKeys = false
		this.controls.screenSpacePanning = false
		this.controls.dampingFactor = .1
//		this.controls.rotateSpeed = 0.2
//		this.controls.panSpeed = 0.4
//		this.controls.minDistance = 0
//		this.controls.maxDistance = Infinity
//		this.controls.minPolarAngle = 0
//		this.controls.maxPolarAngle = 0.99 * Math.PI 
		
		
		this.font = new THREE.Font(require("./fonts/helvetiker_regular.typeface.json"))
		this.textMaterial = new THREE.MeshPhysicalMaterial({
			color: 0x666666, side: THREE.DoubleSide,
			metalness: .15, roughness: 0.4})

		this.nrows = 10;
		this.ncols = 6
		for (var i = 0; i < 10; ++i) {
			for (var j = 0; j < 6; ++j) {
				this.addDatapoint(i, j, Math.random())
			}
		}

		this.addGrid()
		const light = this.addLight()

		wdw.appendChild(this.renderer.domElement)
		// firefox 
//		this.renderer.getContext().getShaderInfoLog = () => ""

		this.rsensor = new ResizeSensor(wdw, this.onResize)
		this.controls.addEventListener("change", this.startRendering)
		
		this.setup(null)
		this.aid = 0
		this.animate()
	},

	beforeDestroy() {
		if (this.$refs.label) this.$refs.label.style.display = "none"
		state.position = this.camera.position
		cancelAnimationFrame(this.aid)
		this.aid = -1
		if (true)
			this.scene.children.forEach(p => {
				if (p.geometry) p.geometry.dispose()
				if (p.material) p.material.dispose()
				this.scene.remove(p)
			})
		if (this.renderer) {
			this.renderer.dispose()
			this.renderer.forceContextLoss()
			if (false) console.table(JSON.stringify(this.renderer.info, 2,2))
		}
		ResizeSensor.detach(this.$refs.scene)
		this.controls.removeEventListener("change", this.startRendering)
	},

	methods: {
		startRendering(event) {
			if(this.rendering) return 0
			this.rendering = true
			this.aid = requestAnimationFrame(this.animate)
		},
		
		animate() {
			this.rendering = false
			this.textPosition()
			
			if (this.cc && this.mc) {
				this.mc.visible = false
				this.cc.update(this.renderer, this.scene)
				this.mc.visible = true
			}
			
			this.controls.update()
			const wdw = this.$refs.scene

			this.renderer.render(this.scene, this.camera)
		},
		
		setup() {
			if (!this.table) return
			cancelAnimationFrame(this.aid)
			
			this.fmtNative = p => ArrowTypes[this.table.measure.type.typeId].asNative(this.table.measure,p)
			this.fmtVisual = p => ArrowTypes[this.table.measure.type.typeId].cellFmt(this.table.measure,{value: p})
			
			const convert = this.fmtNative
			this.clearDatapoints()
			const table = this.table.rows.map(p => p.data)
			const reducer = (a, b) => Math.max(a, b)
			const reducerMin = (a, b) => Math.min(a, b)
			const mapper = p => isNaN(convert(p)) ? 0 : convert(p)
			const min = table.map(r => r.map(mapper).reduce(reducerMin), 0).reduce(reducerMin, 0)
			const max0 = table.map(r => r.map(mapper).reduce(reducer), 0).reduce(reducer, 0)
			
			const max = Math.max(max0, -min)
			this.nrows = this.table.rows.length
			
			if (this.table.cols.length > 0) {
				this.ncols = this.table.cols[0].length

				console.time("label")
				const rdims = this.table.cols[0].map((p, i) => this.table.cols.map(r => r[i]))
				const rlabels = this.table.cols[0].map((p, i) => this.table.cols.map(r => r[i]).join(" - "))
				rlabels.forEach((p, i) => this.writeLabel(true, p, i, this.table.rows.length))

				const cdims = this.table.rows.map(p => p.row)
				const clabels = this.table.rows.map(p => p.row.join(" - "))
				clabels.forEach((p, i) => this.writeLabel(false, p, this.table.cols[0].length, i))
				this.textPosition()
				console.timeEnd("label")

				for (var i = 0; i < table.length; ++i) {
					for (var j = 0; j < table[0].length; ++j) {
						const val = Number.parseFloat(convert(table[i][j]))
						const scaled = 1.0 / max * val
						if (isNaN(val)) continue
						this.addDatapoint(j, i, scaled, table[i][j], cdims[i], rdims[j])
					}
				}
				this.marker(table.length, table[0].length)
			}
			else this.ncols = 0 
			this.animate()
		},
		
		writeLabel(cols, text, x, y) {
			const tmesh = this.getText(text)
			const box = new THREE.Box3().setFromObject(tmesh)

			if (cols) {
				tmesh.rotation.x = -Math.PI / 2
				tmesh.rotation.z = -Math.PI / 2
				this.ctexts.push(({mesh: tmesh, box: box}))
			} else {
				tmesh.rotation.x = -Math.PI / 2
				this.rtexts.push(({mesh: tmesh, box: box}))
			}
			this.scene.add(tmesh)
		},

		clearDatapoints() {
			this.datapoints.forEach(p => {
				if (p.geometry) p.geometry.dispose()
				if (p.material) p.material.dispose()
				this.scene.remove(p)
			})
			this.ctexts.forEach(p => {
				if (p.mesh.geometry) p.mesh.geometry.dispose()
				this.scene.remove(p.mesh)
			})
			this.ctexts = []
			this.rtexts.forEach(p => {
				if (p.mesh.geometry) p.mesh.geometry.dispose()
				this.scene.remove(p.mesh)
			})
			this.rtexts = []
		},

		addDatapoint(x, y, scaled, value, clabels, rlabels) {
			if (isNaN(scaled)) return
			const maxH = 30.0
			
			if (Math.abs(scaled) < 0.0000000001) {
				scaled = 0.0000000001
			}				
			const cv = Math.round(511 * Math.abs(scaled))
			const cp = this.theme[cv][1]
			var material = new THREE.MeshPhysicalMaterial({
				color: new THREE.Color(...cp), flatShading: true, side: THREE.DoubleSide,
				metalness: .65, roughness: 0.4})
			scaled = scaled * maxH
//			var geometry = new THREE.CylinderBufferGeometry(2, 2, scaled, 400, 1)
			var geometry = new THREE.BoxBufferGeometry(4, scaled, 4)
			var mesh = new THREE.Mesh(geometry, material)
			mesh.position.x = x * 10 + 5 - 10 * Math.floor(this.ncols / 2)
			mesh.position.y = scaled / 2 + (scaled > 0? 1.0/maxH/4 : -1/maxH/4) 

			mesh.position.z = y * 10 + 5 - 10 * Math.floor(this.nrows / 2)
			mesh.updateMatrix()
			mesh.matrixAutoUpdate = false
			mesh.castShadow = true
			mesh.receiveShadow = true
			mesh.userData = ({type: "dp", value: this.fmtVisual(value), cols: clabels, rows: rlabels})
			this.scene.add(mesh)
			this.datapoints.push(mesh)
			if (x * y === 1)
				this.tmesh = mesh
		},
		
		onMouseMove(event) {
			event.preventDefault();
			const rect = this.renderer.domElement.getBoundingClientRect();
			this.mouse.x = ((event.clientX - rect.left)/(rect.right-rect.left)) * 2 - 1;		
			this.mouse.y = - ((event.clientY - rect.top)/(rect.bottom - rect.top)) * 2 + 1;
			this.pointer.x = Math.abs(this.pointer.x - event.clientX) > 2 ? event.clientX : this.pointer.x
			this.pointer.y = Math.abs(this.pointer.y - event.clientY) > 2 ? event.clientY : this.pointer.y

			const self = this
			const cp = this.camera.position
			if (this.mouseButton) {
				self.$refs.label.style.zIndex = -10
				return
			}
			this.raycaster.setFromCamera(self.mouse, this.camera);
			const intersects = this.raycaster.intersectObjects(this.scene.children);
			if (this.intersected && this.$refs.label)
				this.$refs.label.style.zIndex = -10
			const wdw = this.$refs.scene
			const toScreen = p => {
				const v = p.project(this.camera)
				return ({
					x: (v.x + 1) / 2 * wdw.clientWidth,
					y: (1 - (v.y + 1) / 2) * wdw.clientHeight
				})
			}
			const w = this.$refs.scene.clientWidth  
			const h = this.$refs.scene.clientHeight

			if (this.sensor && intersects.length > 0 && this.pointerOnScene()) {
				const intersect = intersects.find(p => p.object.userData.type)
				
				if (intersect) {
					const mesh = intersect.object
					this.label.value = mesh.userData.value
					this.label.cols = mesh.userData.cols
					this.label.rows = mesh.userData.rows

					this.intersected = mesh
					const pos = toScreen(intersects[0].point)
					
//					Vue.nextTick().then(() => {
						const rect = this.renderer.domElement.getBoundingClientRect()
							self.$refs.label.style.display = "flex"
							self.$refs.label.style.zIndex = 10
						
//						const clamp = (a,b) => a > b ? - b - 4 : 4
						const clamp = (a,b) => a > b ? - b - 4 : 4 - a
						
						self.$refs.label.style.left = self.pointer.x + clamp(self.pointer.x-rect.x, self.$refs.label.offsetWidth) + "px"
						self.$refs.label.style.top = self.pointer.y + clamp(self.pointer.y-rect.y, self.$refs.label.offsetHeight) + "px"
//					})
				}
			} 
			else if (this.intersected) {
				self.$refs.label.style.display = "none"
				self.$refs.label.style.zIndex = -1000
			}
		},
		
		pointerOnScene () {
			const rect = this.$refs.scene.getBoundingClientRect()
			const point = this.pointer
			return point.x > rect.left 
					&& point.x < rect.left + rect.width
					&& point.y > rect.top 
					&& point.y < rect.top + rect.height
		},
		
		textPosition() {
			const south = (Math.PI/2 < this.controls.getPolarAngle())
			const ncp = this.camera.getWorldDirection(new THREE.Vector3()).projectOnPlane(this.pln)

			const lrs = this.camera.position.x > 0 ? 1 : -1
			const fontAdj = -1.25

			const rpos = (p, n) => {
				const sbox = p.box.getSize(new THREE.Vector3)
				const xalign = (lrs > 0) ? 0 : -sbox.x
				const yalign = -(10 - sbox.y) / 2

				p.mesh.position.x = Math.ceil(lrs * this.ncols / 2) * 10 + xalign + lrs * 2
				p.mesh.position.z = (n + 1 - Math.floor(this.nrows / 2)) * 10 + yalign + fontAdj

				p.mesh.rotation.z = ncp.z > 0 ? Math.PI : 0
				p.mesh.translateX(ncp.z > 0 ? -sbox.x : 0)
				p.mesh.rotation.x = south ? Math.PI/2 :-Math.PI/2
			}

			this.rtexts.forEach((p, n) => rpos(p, n))

			const top = this.camera.position.z > 0 ? 1 : -1

			const cpos = (p, n) => {
				const sbox = p.box.getSize(new THREE.Vector3)
				const xalign = (top > 0) ? 0 : -sbox.x
				const yalign = -(10 - sbox.y) / 2

				p.mesh.position.x = (n + 1 - Math.floor(this.ncols / 2)) * 10 + yalign + fontAdj
				p.mesh.position.z = Math.ceil(top * this.nrows / 2) * 10 + xalign + top * 2

				p.mesh.rotation.z = ncp.x < 0 ? Math.PI / 2 : -Math.PI / 2
				p.mesh.translateX(ncp.x < 0 ? -sbox.x : 0)
				p.mesh.rotation.y = south ? Math.PI :0
			}

			this.ctexts.forEach((p, n) => cpos(p, n))
			
			const kcp = this.camera.position
			const limit = 250
			
			const submerged = Math.abs(kcp.x) < limit && Math.abs(kcp.z) < limit && kcp.y < 0 
			this.platform.u.visible = !submerged
			this.platform.d.visible = submerged
		},

		marker(x, y) {
//			if (true) return
			const self = this
			const msz = 1.0
			const pos = () => {
				self.obj.position.y = 0
				self.obj.position.x = (-1*Math.floor(y/2+1)*10) + 5
				self.obj.position.z = (-1*Math.floor(x/2+1)*10) + 5
			}
			if (!this.obj) {
			const material = 
//					new THREE.MeshPhongMaterial({color: 0x999999, depthWrite: true,
//				transparent:true, side: THREE.DoubleSide,opacity:.85})
				new THREE.MeshPhysicalMaterial({
				color: new THREE.Color(.44,.42,.4), flatShading: false,side: THREE.DoubleSide, opacity: .8, 
				transmission: .8, transparent: true,
				metalness: .15, roughness: 0.4})			
//			var geometry = new THREE.CylinderBufferGeometry(2, 2, scaled, 400, 1)
//			const geometry = new THREE.BoxBufferGeometry(5, .1, 5)
			const geometry = new THREE.SphereBufferGeometry(msz, 128, 128)
			this.obj = new THREE.Mesh(geometry, material)
			this.obj.castShadow = true
			this.scene.add(this.obj)
			}
			pos()
			

		},
		addGrid() {
			
			const d = 4
			var geometry = new THREE.BoxBufferGeometry(500, 500, d)
			var material = new THREE.MeshPhongMaterial({color: 0x999999, depthWrite: true,
				transparent:true, side: THREE.FrontSide,opacity:.7})
//			var material = new THREE.MeshPhysicalMaterial({
//				color: 0x999999, flatShading: true, side: THREE.DoubleSide,
//				metalness: .65, roughness: 0.4})

			this.platform.u = new THREE.Mesh(geometry, material)
			this.platform.u.position.set(0, -d/2, 0)
			this.platform.u.rotation.x = -Math.PI / 2
			this.platform.u.receiveShadow = true
			this.scene.add(this.platform.u)
			
			geometry = new THREE.PlaneBufferGeometry(500, 500)
			material = new THREE.MeshPhongMaterial({color: 0x999999, depthWrite: true,
				transparent:true, side: THREE.DoubleSide,opacity:.85})

			this.platform.d = new THREE.Mesh(geometry, material)
			this.platform.d.position.set(0, 0, 0)
			this.platform.d.rotation.x = -Math.PI / 2
			this.platform.d.receiveShadow = true
			this.platform.d.visible = false
			this.scene.add(this.platform.d)

			var grid = new THREE.GridHelper(500, 50, 0x000000, 0x000000)
			grid.position.y = 0
			grid.material.opacity = 0.2
			grid.material.transparent = true
			grid.receiveShadow = false

			this.scene.add(grid)

			this.pln = new THREE.Vector3(0, 1, 0)

			const points = [
				new THREE.Vector3(-200, 0, 0),
				new THREE.Vector3(200, 0, 0)
			]
			var geometry = new THREE.BufferGeometry().setFromPoints(points)
			var material = new THREE.LineBasicMaterial({color: 0x885500,side: THREE.FrontSide,})
			this.scene.add(new THREE.Line(geometry, material))

			const points1 = [
				new THREE.Vector3(0, 0, -200),
				new THREE.Vector3(0, 0, 200)
			]
			var geometry1 = new THREE.BufferGeometry().setFromPoints(points1)
			var material1 = new THREE.LineBasicMaterial({color: 0x004488,side: THREE.FrontSide,})
			this.scene.add(new THREE.Line(geometry1, material1))
		},
		
		addLight() {
			var light0 = new THREE.SpotLight(0xffffff)
			light0.position.set(-100, 100, 400)
			light0.castShadow = true;
			this.scene.add(light0)

			var light = new THREE.DirectionalLight(0x222222)
			light.position.set(600, 0, 600)
			light.castShadow = true
			this.scene.add(light)

			light = new THREE.AmbientLight(0xddddddcc)
			this.scene.add(light)
			return light0
		},
		onResize() {
			const wdw = this.$refs.scene
			if (!wdw) return
			this.camera.aspect = wdw.clientWidth / wdw.clientHeight
			this.camera.updateProjectionMatrix()
			this.renderer.setSize(wdw.clientWidth, wdw.clientHeight)
			this.startRendering()
		},
		centerView() {
			this.controls.target = new THREE.Vector3(0,0,0)
			this.startRendering()
		},
		onKeyPress(x) {},
		takePicture1() {
			const svgr = new SVGRenderer()
			const wdw = this.$refs.scene
			svgr.setSize(wdw.clientWidth, wdw.clientHeight)
			svgr.setClearColor(0xffffff);
			svgr.setQuality('high')
			svgr.render(this.scene, this.camera)
			const z = svgr.domElement
			console.log(z.outerHTML)
			
			var header = '<?xml version="1.0" standalone="no"?>\r\n';
			var blob = new Blob([header, z.outerHTML], {type:"image/svg+xml;charset=utf-8"});
			FileSaver.saveAs(blob, "scene.svg");
		},
		takePicture(){
			const dataURI = this.renderer.domElement.toDataURL("image/png")
			const binStr = window.atob(dataURI.split(",")[1] );
			const len = binStr.length;
			const arr = new Uint8Array(len);
			for ( let i = 0; i < len; i++ ) arr[i] = binStr.charCodeAt(i);
			const blob = new window.Blob([arr]);
			FileSaver.saveAs(blob, "scene.png");
		},

		onClick(event) {
			if (!event.ctrlKey) return
			const ncp = this.camera.getWorldDirection(new THREE.Vector3()).projectOnPlane(this.pln)
			const nlp = this.camera.position
			console.log(ncp)
		},

		getText(txt) {
			var shapes = this.font.generateShapes(txt, 2);

			var extrudeSettings = {
				steps: 1,
				depth: .1,
				bevelEnabled: false,
				bevelThickness: 1,
				bevelSize: 1,
				bevelSegments: 1
			};

			var geometry = false
					? new THREE.ExtrudeBufferGeometry(shapes, extrudeSettings)
					: new THREE.ShapeBufferGeometry(shapes)
//			const posx = -0.5 * (geometry.boundingBox.max.x - geometry.boundingBox.min.x);
//			geometry.computeBoundingBox();
			geometry.translate(0, -1, 0.01);
			const mesh = new THREE.Mesh(geometry, this.textMaterial);
			return mesh
		},
	}
}

