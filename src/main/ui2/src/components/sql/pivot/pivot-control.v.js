/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import Vue from "vue";
import ScrollTable from "./ScrollTableDnD";
import splitpane from "@/components/common/splitpane";
import DtIcon from "@/components/icons/dt-icon"
import {predicate,Type,Table} from "apache-arrow";

import nselect from "./nselect"
import draggable from "vuedraggable";
import cloneDeep from "lodash/cloneDeep";
import isEqual from "lodash/fp/isEqual";
import _ from "lodash/collection";
export default {
	name: "pivot-control",
	components: {ScrollTable,nselect,draggable,splitpane, DtIcon},
	data: () => ({
		pos: 60,
		dataTable: null,
		mode: "ALL_WITH_INDETERMINATE",
		model: {
			cols: [""],
			rows: []
		},
		measures: {
			cols: ["Measure"],
			rows: []
		},
		pivot: null,
		dimensions: {},
		prev: {
			struct: {},
			data: [],
			measure: null
		},
		pdata: [],
		cdims: [],
		rdims: [],
		vdims: [],
		dkey: {},
		schema: {},
		ntz: 0,
		filter: null,
		lock: false,
		modelf: {cols:["Columns"], rows:[]}
	}),

	props: {
		table: Object,
		sql: null
	},

	watch: {
		filter(n,o) {
			this.modelc()
		},
		table(n,o) {
			this.prev.struct = null
			this.init()
		},
	},

	mounted() {
		this.init()
//		const split = Split([this.$refs.left, this.$refs.right], {sizes: [60, 40], gutterSize:2})
	},

	computed: {
		dimkey() {
			return Object.keys(this.dimensions)
		},
	},
	created() {},
	updated() {},
	beforeDestroy() {},

	methods: {
		sclose() {
			this.$refs.left.scrollLeft = 0
		},
		modelc() {
			const matched = p => 
					(this.filter && this.filter.length > 0)
					&& p.trim().toLowerCase().indexOf(this.filter.trim().toLowerCase()) > -1 
					&& !this.cdims.includes(p[0])
			const used = this.cdims.concat(this.rdims).concat(this.vdims)
			const [head,tail] = _.partition(this.model.rows.filter(p =>!used.includes(p)), p => matched(p[0]))
			head.forEach(p => p[1]=true)
			tail.forEach(p => p[1]=false)
			this.modelf = {
				cols: ["Columns"],
				rows: head.concat(tail)
			}
		},
		sqlType(field) {
			const f = this.table.getColumn(field)
			if (!f) return ""
			switch (f.typeId) {
				case Type.Float:
				case Type.Decimal: return "decimal"
				case Type.Date: 
				case Type.Timestamp: return "date"
				case Type.Int: return "integer"
				case Type.Bool:	return "boolean"
				case Type.Utf8: return "string"
				default: return ""
			}
		},
		dims(x) {
			const dim = x[0]
			return this.dimensions[dim]
		},
		dimKey(x) {
			const dim = x[0]
			const k = this.dimensions[dim] ? this.dimensions[dim].key : 0
			console.log("key is", k)
			return k
		},
		// refactor this
		fmt(atype, quoted=true) {
			const quote = p => quoted ? "'" +p + "'" : p
			if (atype.typeId===7) {
				return p => {
					const scale = atype.scale
					const vc = Math.pow(10, -scale) * p
//						return Intl.NumberFormat("en-US", 
//								{useGrouping: true,minimumFractionDigits:scale,maximumFractionDigits:scale}).format(vc)
					return vc		
				}
			}
			if (atype.typeId===5) return p => quote(p)
			if (atype.typeId===8) return p =>
					p && p.toISOString ? quote(p.toISOString().slice(0,10)) : null

			return p => p
		},
		test() {},
		init() {
			this.cdims = []
			this.rdims = []
			this.vdims = []
			this.$emit("pivot", {measure: "", cols:[], rows: []})

			if (this.table) {
				this.model.rows = this.table.schema.fields.map(p=>[p.name, 0])
				this.table.schema.fields.forEach(p=> {
					this.dkey[p.name] = "-"+p.name
					this.schema[p.name] = ({
						name: p.name,
						id: p.name,
						fmt: v => this.fmt(p.type)(v),
						aggregators: ["sum","min","max","avg","stddev"].map((p,i)=>({id:i, label:p})),
						aggregator: null,
					})
				})
			}
			this.modelc()
		},
		nxt(f, ...args) { this.$nextTick().then(() => f(...args)) },
		onSelect1(x, ids) {
			return this.nxt(this.onSelect1, x, ids)
		},
		onSelect(x, ids) {
			const dim = x[0]
			this.addDimension(dim).then(dim => {
				dim.selects = ids.sort((a,b) => a-b)
				console.log("ON SELECT", dim)
				this.mkPivot()
			})
		},
		onAdd(event) {
			const dim = event.item.cells[0].textContent
			console.log("ON add", event)
			this.addDimension(dim).then(() => {
				this.cdims.concat(this.rdims)
						.filter(p => p[0]===dim)
						.forEach(p => this.$set(p, 1, dim + new Date().getTime()))
			})
		},
		onChange(event) {
		},
		onEnd(event) {
			this.ntz += 1
//				event.item.lastChild.style.display = "table-cell"
		},
		onStart(event) {
//				event.item.lastChild.style.display = "none"
		},
		async run(dim) {
			this.$session.activity = true
			const sql = `select distinct(${dim}) from (\n ${this.sql} \n)`
			const r = await this.$svr.executeSqlx(sql, false).catch(x => console.log(x))
			this.$session.activity = false
			return Table.from(r.data) 
		},
		async addDimension(dim) {
			if (this.dimensions[dim]) return this.dimensions[dim]
			this.dkey[dim] = 0
			const table = await this.run(dim)
			
				const col = table.getColumn(dim)
				const fmt = this.$session.colFormat(col)
				const comparator = (a,b) => {
					const af = fmt(a)
					const bf = fmt(b)
					return af===bf ? 0 : (af > bf ? 1 : -1)
				}
				
				if (col) {
					const distincts = [...col].reduceRight((o,p)=>{o.add(p); return o}, new Set())
					const all = [...distincts].sort((a,b) => typeof(a)==="number"&&typeof(b)==="number" ? a-b : a>b)
					const highCardinality = all.length > 1000
					const loader = (x,y) => {
						const sq = x.searchQuery.toLowerCase()
						const r = all.map((p,i) => ({id:i, label:fmt(p)})).filter(p => ((""+p.label).toLowerCase()).match(sq))
						if (r.length > 1000 ) x.callback("Too much data: "+r.length,[])
						else x.callback(null, r)
					}
					this.dimensions[dim] = ({
						name: dim,
						idx: col.field.name,
						async: highCardinality,
						loader: loader,
						values: all,
						all: highCardinality 
							? all.map((p,i)=>({id:i, label:fmt(p), value:p})) 
							: all.map((p,i)=>({id:i, label:fmt(p), value:p})),
						aggregators: ["sum","count", "average", "max", "min"].map((p,i)=>({id:i, label:p})),
						quote: this.$session.colQuote(col), 
						selects: [],
						key: new Date().getTime()
					})
					Vue.set(this.dkey,dim,dim)
				}
		},
		mkPivot() {
			console.log("mkPivot", new Date)
			const cdims = this.cdims.map(p => p[0])
			const rdims = this.rdims.map(p => p[0])
			var measure = "count(*)"
			if (this.vdims[0]) {
				const vdim = this.vdims[0][0]
				const mcol = this.schema[vdim]
					measure = mcol.aggregators[mcol.aggregator].label+"("+mcol.name+")"
			}

			const alldims = cdims.concat(rdims)
			if (alldims.length < 1) {
				const result = ({measure: {name:measure}, cpivots:cdims, rpivots:rdims, cols:[], rows: []})
				this.$emit("pivot", result);

				return 0
			}
			const struct = {}
			var ready = true
			alldims.forEach(p => {
					if (this.dimensions[p])
						struct[p] = this.dimensions[p].selects.slice(0)
					else ready = false
			})
			
			if (!ready) return 0
			const newStruct = !isEqual(struct, this.prev.struct)
			this.prev.struct = cloneDeep(struct)

			const dimv = p => p.selects.map(i => p.values[i]).map(n=>n!==null&&typeof(n)!=="boolean"?n.toString():n).map(n => predicate.col(p.name).eq(n))
			const dims = p => p.selects.filter(i => p.all[i].value!==null).map(i => p.quote(p.all[i].label))
			const dimn = p => p.selects.map(i => p.all[i].label)

			const dimsN = p => p.selects.filter(i => !p.all[i].value===null).length

			const buildPred = p => {
				const s0 = dims(p).length && dimsN(p) ? "(" : ""
				const s1 = dims(p).length ? p.name +" in ("+dims(p).join(",")+")" :""

				const s2 = (dims(p).length && dimsN(p) ? " or ": "") 
				const s3 = dimsN(p) ? p.name + " is null" : ""
				const s4 = dims(p).length && dimsN(p) ? ")" : ""
				return s0+s1+s2+s3+s4
			} 

			const hasSelects = p => p.selects.length
			const groupdims = alldims.map(p=>this.dimensions[p]).filter(hasSelects)
			const groupnames = groupdims.map(p=>p.name)
			const pred = groupdims.map(buildPred).join(" and ")
			
			if (!groupnames.length) {
				const result = ({measure: "", cpivots:cdims, rpivots:rdims, cols:[], rows: []})
				this.$emit("pivot", result);
				this.$session.activity = false
				return 0
			}

			const sql = "select "+groupnames.join(",")+","+measure+" from (\n"+this.sql+"\n)"
				+ " where "+pred + " group by "+groupnames.join(",")

			const self = this
			const query = q => (newStruct || measure!==this.prev.measure)
					? this.$svr.executeSqlx(q, false)
							.then(p=> {
									const table = Table.from(p.data)
									self.$bus.$emit("show-stats", ({tm: p.headers.timing, nr: table.count(), mem: p.data.byteLength}))
									this.prev.data=p.data.slice();
									this.prev.measure=measure; 
									return table
					})
					: Promise.resolve(Table.from(this.prev.data))


			this.$session.activity = true
			query(sql).then(table => {
				const measureField = table.schema.fields[table.schema.fields.length-1]
				const measureCol = measureField.name

				const filter = (a,b) => a.filter(b)
				const spread = (a,b) => [...a, b]

				const f0 = t => op => (h,...r) => h ? h.flatMap(p => f0(op(t,p))(op)(...r)) : [t]

				const group = t => p => f0(t)(filter)(...p)
				const cartesian = p => f0([])(spread)(...p)

				const rows = group(table)(rdims.map(p=>this.dimensions[p]).filter(hasSelects).map(dimv))

				const tbl = rows.map(p => group(p)(cdims.map(p=>this.dimensions[p]).filter(hasSelects).map(dimv)).map(p => {
					if (!measure) return p.count()
					const result = []
					var col = []
					p.scan(idx => {result.push(col(idx))}, batch => col = predicate.col(measureField.name).bind(batch))
					const z = result.reduce((a,b) => b, 0)
					return (z)
				}))

				const rh = cartesian(rdims.map(p=>this.dimensions[p]).filter(hasSelects).map(dimn))
				const ch = cartesian(cdims.map(p=>this.dimensions[p]).filter(hasSelects).map(dimn))
				const transpose = p => Object.keys(p[0]).map(cn => p.map(rn => rn[cn]))
				console.log("ch", this.dimensions)
				const cols = transpose(ch)
				
				const cdims1 = cdims.length>0 ? cdims : [""]
				const cols1 = cdims.length>0 ? cols : [[""]]
				
				const result = ({measure: measureField, cpivots:cdims1, rpivots:rdims, cols:cols1, rows: rh.map((p,i) => ({row:p, data:tbl[i]}))})
				this.$emit("pivot", result);
				console.log("result", result)
				this.$session.activity = false
			})
		},
	}
}


