/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import antlr4 from "antlr4";
import {SqlBaseLexer} from "./g4/SqlBaseLexer";
import {SqlBaseParser} from "./g4//SqlBaseParser";
import CodeMirror from "codemirror";
import sfuncs from "./spark-functions";

export default new class {
	constructor() {
		this.lexer = new SqlBaseLexer()
		this.symbols = this.lexer.symbolicNames
		this.keywords = this.lexer.symbolicNames
				.filter(p => p && p.length > 1)
				.map(p => p.toLowerCase())
//				.filter(p=>p.endsWith("value"))
		this.keywords.push("double", "integer", )
	}
	
	run(cm, sql, opt) {
		var chars = new antlr4.InputStream(sql);

		const pos = cm.getCursor()
		const Pos = CodeMirror.Pos
		const token = cm.getTokenAt(pos)
		const d = pos.ch - token.start
		const part = token.string.substring(0, d)
		const keyws = this.keywords.filter(p => p.startsWith(part)).map(p => ({text: p, className: "CodeMirror-hint-keyword"}))
		const funcs = sfuncs.filter(p => p.startsWith(part)).map(p => ({text: p, className: "CodeMirror-hint-function"}))
		const hints = keyws.concat(funcs)
		const r = {list: hints, from: Pos(pos.line, token.start), to: Pos(pos.line, pos.ch)}
		return r
	}
}