/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import axios from "axios";

class Server {
	baseUrl = ""
	join(...p) {
		return p.join("/")
	}
	
	static sparkUI() {
		return axios.get("/api/v1/dict")
	}
	static status() {
		return axios.get("/api/v1/dict/status")
	}
	
	static setLimit(value) {
		return axios.post("api/v1/dict/limit", value)
	}
	static setSchema(value) {
		return axios.post("api/v1/dict/schema", value)

	}
	static reset() {
		return axios.get("/api/v1/dict/reset")
	}
	static bstr() {
		return axios.get("/api/v1/dict/bstr", {responseType:'arraybuffer'})
	}
	static executeSqlx(sql, limited=true, dbg=true) {
		return axios.get("/api/v1/dict/sqlx", {responseType:'arraybuffer', params: {q: sql, limited: limited, dbg:dbg}})
	}
	static executeSql(sql) {
		return axios.get("api/v1/dict/sql", {params: {q: sql}})
	}
	static importJdbc(sql, jdbc, user, pwd, tbl) {
		return axios.get("/api/v1/dict/importJdbc", {params: {q: sql, url:jdbc, u:user, p:pwd, tbl:tbl}})
		
	}
	static repoList(path) {
		return axios.get("/api/v1/repo/list")
	}
	static repoGet(path) {
		return axios.get(`/api/v1/repo/get/${path}`)
	}
	static repoSave(path, document, msg=null) {
		return msg 
		? axios.post(`/api/v1/repo/com/${path}`, document, {params:{msg:msg}})
				.then(p => console.log("###", "script saved"))
		: axios.post(`/api/v1/repo/set/${path}`, document)
				.then(p => console.log("###", "script saved"))
	}
	static repoNew(path) {
		return axios.get(`/api/v1/repo/ren`, {params: {f:path}})
	}
	static repoRename(of, nf) {
		return axios.get(`/api/v1/repo/ren`, {params: {f:of, n:nf}})
	}
	static repoDelete(path) {
		return axios.get(`/api/v1/repo/del`, {params: {f:path}})
	}
	
	static logout() {
		return axios.get("/auth")
	}
	static getSchemaNodes(node) {
		switch (node.type) {
			case 0:
				return axios.get(`/api/v1/dict/schemas`).then(r => r.data.map(p =>new SchemaNode(node, p)))
			case 1: 
				return axios.get(`/api/v1/dict/schemas/${node.path.join("/")}`).then(r => r.data.map(p =>new SchemaNode(node, p)))
			case 2: 
				return axios.get(`/api/v1/dict/schemas/${node.path.join("/")}`).then(r => node.subNodes(r.data, _ => Server.getSchemaNodes(node)))
			default: return []	
		}
	}
	
	static getTableInfo(db, tbl) {
		return axios.get("/api/v1/dict/schemas/" + db + "/" + tbl + "/info")
	} 
}

export default Server
	
	
