/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import axios from "axios";

class FMT {
	static upper = v => v[0].toUpperCase() + v.slice(1)
	static intFmt = p => Intl.NumberFormat("en-US", {useGrouping: true}
		).format(p)
	static sizeFmt = p => {
		const int1Fmt = p => Intl.NumberFormat("en-US", {useGrouping: false, minimumFractionDigits: 1, maximumFractionDigits: 1}).format(p)
		const log = Math.log10(p)
		if (log < 3) return p + " bytes"
		else if (log < 6) return int1Fmt(p / 1024) + " KB"
		else if (log < 9) return int1Fmt(p / 1024 / 1024) + " MB"
		else return int1Fmt(p / 1024 / 1024 / 1024) + " GB"
	}
}

class TreeNode {
	path = []
	parent = null
	subNodes = []
	tid = 0

	open = false
	busy = false

	prev = null
	next = null

	constructor(parent, name, tid, data = null) {
		const p = name ? [name] : []
		this.path = (parent) ? parent.path.concat([name]) : p
		this.tid = tid
		this.parent = parent ? parent : this
		this.data = data
		this.prev = this
		this.next = this
	}
	mktid() {
		return Date.now()
	}
	fqn() {
		return this.path.join(".")
	}
	name() {
		return this.path.slice(-1)[0]
	}
	key() {
		return this.path.concat(this.tid).join(".")
	}
	level() {
		return this.path.length
	}

	linkList() {
		const nodes = [this].concat(this.subNodes)
		this.subNodes.forEach((p, i) => {
			p.prev = nodes[i]
			if (i > 0) nodes[i].next = p
		})
		nodes[nodes.length - 1].next = this.next === this ? nodes[nodes.length - 1] : this.next
	}
	reloadInitial() {
		if (!this.data) this.reload()
	}
	reload() {
		this.busy = true
		return axios.get(`/api/v1/dict/schemas`).then(r => {
			this.subNodes = r.data.map(p => new SchemaNode(this, p.name, this.mktid(), p))
			this.linkList()
			this.busy = false
		})
	}
	info() {
		return []
	}
}

class SchemaNode extends TreeNode {
	type() {
		return "library"
	}
	reload() {
		this.busy = true
		return axios.get(`/api/v1/dict/schemas/${this.path.join("/")}`).then(r => {
			this.subNodes = r.data.map(p => new TableNode(this, p.name, this.mktid()))
			this.linkList()
			this.busy = false
		})
	}
	info() {
		return Object.keys(this.data).map(p => [FMT.upper(p), this.data[p]])
	}
}

class TableNode extends TreeNode {
	type() {
		return "table"
	}

	reload() {
		this.busy = true
		return axios.get(`/api/v1/dict/schemas/${this.path.join("/")}`).then(r => {
			this.data = r.data
			this.subNodes = r.data.columnInfos.map(p => new ColumnNode(this, p.field.name, this.mktid(), p))
			this.linkList()
			this.busy = false
		})
	}
	info() {
		if (!this.data) return []
		const r = []
		const d = this.data

		const keys = Object.keys(d)
		r.push(["Table", d.identifier.table])
		r.push(["Schema", d.identifier.database])
		const plain = p => !["identifier", "columnInfos", "stats"].includes(p)
		keys.filter(plain).forEach(p => r.push([FMT.upper(p), d[p]]))

		if (d.stats) {
			const rc = "rowCount"
			Object.keys(d.stats).filter(p => p === rc).forEach(p => r.push([FMT.upper(p), FMT.intFmt(d.stats[p])]))
			const sz = "sizeInBytes"
			Object.keys(d.stats).filter(p => p === sz).forEach(p => r.push(["StorageSize", FMT.sizeFmt(d.stats[p])]))
		}

		return r
	}
}
class ColumnNode extends TreeNode {
	type() {
		return this.data.field.type
	}
	reload() {
		this.busy = true
		return axios.get(`/api/v1/dict/schemas/${this.parent.path.join("/")}`).then(r => {
			const x = r.data.columnInfos
					.filter(p => p.field.name === this.name())
					.forEach(p => {
						this.data = p;
						this.tid = this.mktid()
					})
			this.busy = false
		})
	}
	info() {
		if (!this.data) return []
		const r = []
		const d = this.data

		if (d.field) {
			const fn = "name"
			r.push([FMT.upper(fn), d.field[fn]])
			const tp = "type"
			r.push([FMT.upper(tp), d.field[tp]])
			r.push(["IsPartition", d["partition"]])

			if (d.stats) {
				const rows = Object.keys(d.stats).filter(p => p !== "histogram").map(p => [FMT.upper(p), d.stats[p]])
				r.push(...rows)
			}
		}
		return r
	}

}

class SparkNode extends TreeNode {
	type() {
		return "database"
	}
	constructor() {
		super()
		this.tid = this.mktid()
	}
	name() {
		return "Spark Datawarehouse"
	}
	info() {
		return [["Name", this.name()]]
	}
}

export default SparkNode