/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import svr from "./server";
import {Type} from "apache-arrow";

import SparkNode from "./schema";
import Vue from "vue";

class PanelState {
	pos = 10
	prev = -1
	rev = false
	constructor(initial, rev) {
		this.rev = (rev) ? rev : false
		if (initial) this.pos = initial
	}
	toggle() {
		console.log("TOGGLE", this.pos, this.prev)
		if (this.prev < 0) {
			this.prev = this.pos
			this.pos = this.rev ? 99.9 : 0.1
	}
		else {
			this.pos = this.prev
			this.prev = -1
		}
	}
}

class Session {
	treeNode = new SparkNode
	item = this.treenode
	itemv = this.treenode
	shiftPressed = false
	activity = false
	panel0 = new PanelState(20)
	panel1 = new PanelState(40, true)
	svr = svr
	
	static formatters = ({
		[Type.Date]: val => val && val.toISOString ? val.toISOString().slice(0, 10) : null
	})

	onKeyEvent(event) {
		if ("keydown" === event.type && event.keyCode === 16) this.shiftPressed = true
		else if ("keyup" === event.type && event.keyCode === 16) this.shiftPressed = false
	}
	isShiftPressed() {
		return this.shiftPressed
	}
	fullScreen() {
		this.panel0.toggle()
		this.panel1.toggle()
	}

	colQuote(col) {
		return p => [Type.Date, Type.Utf8].includes(col.typeId) ? "'" + p + "'" : p
	}

	colFormat(col) {
		const fmt = this.constructor.formatters[col.typeId]
		return fmt ? fmt : v => v === null ? "NULL" : v.toString()
	}
}
export default new Session
