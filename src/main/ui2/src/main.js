/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import Vue from 'vue';
import App from './App.vue';

import "@openfonts/roboto_latin";
import "@openfonts/roboto-mono_latin";
import "@openfonts/open-sans_latin";

import Zondicon from 'vue-zondicons';
Vue.component('Zondicon', Zondicon);

import "@/assets/local.css"

import Server from "@/model/server";
import session from "@/model/session";

import Vk from "@/components/common";
Vue.use(Vk);

import hljs from "highlight.js";
Vue.use(hljs.vuePlugin);

Vue.config.productionTip = false

Vue.prototype.$svr = Server
Vue.prototype.$bus = new Vue()

Vue.prototype.$session = Vue.observable(session)

new Vue({render: h => h(App)}).$mount('#app')

