/*
 * Copyright (c) 2017-2021 Jochen Schwenk, Axio Components for Business.
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.axiohq.spark.svr
import scala.io.Source

import java.io.File
import java.nio.file.Paths
import java.sql.Connection
import java.sql.DriverManager
import org.apache.commons.io.FileUtils
import org.apache.log4j.PropertyConfigurator
import zio.ExitCode
import zio.ZIO

import scala.collection.JavaConverters._

class DistMain
object DistMain extends zio.App with Logging {
	def run(args: List[String]) = {
		CommandLine(args) match { 
			case Some(_) => 
				val csrc = classOf[DistMain].getProtectionDomain.getCodeSource
				val base = Paths.get(csrc.getLocation.getFile).getParent.resolve("data")
				val path = base.toString
				System.setProperty("qf.dist", base.toString)

				Seq("conf","lib", "db", "dwh", "tmp", "log").map(new File(base.toFile, _)).foreach(_.mkdirs)
				
				val logcf = s"${path}/conf/log4j.properties"
				if (! Paths.get(logcf).toFile.exists ) {
					val lines = Source.fromResource("templates/log4j.properties", this.getClass.getClassLoader)
							.getLines
					FileUtils.writeLines(new File(logcf), lines.toSeq.asJava)
				}
				
				PropertyConfigurator.configure(logcf)

//				Server.createWebServer().start
				var con: Connection = null
				try {
					con = DriverManager.getConnection(s"jdbc:h2:${path}/db/qf", "qf", "qf")
					con.createStatement.execute("create schema if not exists spark")
					con.createStatement.execute("create table if not exists spark.roles (role_id bigint, create_time bigint, owner_name varchar(128), role_name varchar(128))")
					//			con.createStatement.execute("shutdown")
				} catch {
					case x: Exception => x.printStackTrace
				} finally if (con != null) con.close
				Runtime.getRuntime.addShutdownHook(new Thread {
					override def run = {
						con = DriverManager.getConnection(s"jdbc:h2:${path}/db/qf", "qf", "qf")
						con.createStatement.execute("shutdown")
						con.close
						LOG.info("DB closed")
					}
				})
				Main.run(args)
			case None => ZIO.succeed(ExitCode.failure)
		}
	}
}
