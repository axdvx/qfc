/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.axiohq.spark.svr

import org.apache.arrow.memory.RootAllocator
import org.apache.arrow.vector.types.pojo.ArrowType
import scala.collection.JavaConverters._
import org.apache.arrow.vector.types.{DateUnit, FloatingPointPrecision, TimeUnit}
import org.apache.arrow.vector.types.pojo.{ ArrowType, Field, FieldType, Schema }
import org.apache.arrow.vector.complex.MapVector
import org.apache.spark.sql.types._
import org.apache.arrow.vector.ipc.ArrowStreamWriter
import org.apache.arrow.vector.ipc.message.IpcOption
import org.apache.arrow.vector.ipc.WriteChannel
import org.apache.spark.sql.execution.SQLExecution
import org.apache.spark.sql.{Dataset,Row}

object DecimalType1 extends DecimalType {
	object Fixed {
		def unapply(t: DecimalType): Option[(Int, Int)] = Some((t.precision, t.scale))
	}
}

object ArrowFunctions {
	def sqlExecution[T](ds: Dataset[Row])(body: => T) = SQLExecution.withNewExecutionId(ds.queryExecution, Some("arrow"))(body)
	
	def writeEndOfArrowStream(channel: WriteChannel) = ArrowStreamWriter.writeEndOfStream(channel, new IpcOption)
	val rootAllocator = new RootAllocator(Long.MaxValue)

	// todo: support more types.

	/** Maps data type from Spark to Arrow. NOTE: timeZoneId required for TimestampTypes */
	def toArrowType(dt: DataType, timeZoneId: String): ArrowType = dt match {
		case BooleanType => ArrowType.Bool.INSTANCE
		case ByteType => new ArrowType.Int(8, true)
		case ShortType => new ArrowType.Int(8 * 2, true)
		case IntegerType => new ArrowType.Int(8 * 4, true)
		case LongType => new ArrowType.Int(8 * 8, true)
		case FloatType => new ArrowType.FloatingPoint(FloatingPointPrecision.SINGLE)
		case DoubleType => new ArrowType.FloatingPoint(FloatingPointPrecision.DOUBLE)
		case StringType => ArrowType.Utf8.INSTANCE
		case BinaryType => ArrowType.Binary.INSTANCE
		case DecimalType1.Fixed(precision, scale) => new ArrowType.Decimal(precision, scale)
		case DateType => new ArrowType.Date(DateUnit.DAY)
		case TimestampType =>
			if (timeZoneId == null) {
				throw new UnsupportedOperationException(
					s"${TimestampType.catalogString} must supply timeZoneId parameter")
			} else {
				new ArrowType.Timestamp(TimeUnit.MICROSECOND, timeZoneId)
			}
		case _ =>
			throw new UnsupportedOperationException(s"Unsupported data type: ${dt.catalogString}")
	}

	/** Maps field from Spark to Arrow. NOTE: timeZoneId required for TimestampType */
	def toArrowField(
		name: String, dt: DataType, nullable: Boolean, timeZoneId: String): Field = {
		dt match {
			case ArrayType(elementType, containsNull) =>
				val fieldType = new FieldType(nullable, ArrowType.List.INSTANCE, null)
				new Field(name, fieldType,
					Seq(toArrowField("element", elementType, containsNull, timeZoneId)).asJava)
			case StructType(fields) =>
				val fieldType = new FieldType(nullable, ArrowType.Struct.INSTANCE, null)
				new Field(name, fieldType,
					fields.map { field =>
						toArrowField(field.name, field.dataType, field.nullable, timeZoneId)
					}.toSeq.asJava)
			case MapType(keyType, valueType, valueContainsNull) =>
				val mapType = new FieldType(nullable, new ArrowType.Map(false), null)
				// Note: Map Type struct can not be null, Struct Type key field can not be null
				new Field(name, mapType,
					Seq(toArrowField(
						MapVector.DATA_VECTOR_NAME,
						new StructType()
							.add(MapVector.KEY_NAME, keyType, nullable = false)
							.add(MapVector.VALUE_NAME, valueType, nullable = valueContainsNull),
						nullable = false,
						timeZoneId)).asJava)
			case dataType =>
				val fieldType = new FieldType(nullable, toArrowType(dataType, timeZoneId), null)
				new Field(name, fieldType, Seq.empty[Field].asJava)
		}
	}

	/** Maps schema from Spark to Arrow. NOTE: timeZoneId required for TimestampType in StructType */
	def toArrowSchema(schema: StructType, timeZoneId: String): Schema = {
		new Schema(schema.map { field =>
			toArrowField(field.name, field.dataType, field.nullable, timeZoneId)
		}.asJava)
	}

}
